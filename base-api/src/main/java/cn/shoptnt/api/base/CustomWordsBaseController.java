/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.errorcode.GoodsErrorCode;
import cn.shoptnt.model.goodssearch.EsSecretSetting;
import cn.shoptnt.service.goodssearch.CustomWordsManager;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 自定义分词控制器
 *
 * @author liuyulei
 * @version v1.0
 * @since v7.0.0
 * 2019-05-26
 */
@RestController
@RequestMapping("/base/load-customwords")
@Tag(name = "加载分词库")
public class CustomWordsBaseController {

    @Autowired
    private CustomWordsManager customWordsManager;
    @Autowired
    private SettingClient settingClient;

    @GetMapping
    @Parameters({
            @Parameter(name = "secret_key", description = "秘钥", required = true,   in = ParameterIn.QUERY)

    })
    public String getCustomWords(@Parameter  String secretKey){

        if(StringUtil.isEmpty(secretKey)){
            return "";
        }
        String value = settingClient.get(SettingGroup.ES_SIGN);
        if(StringUtil.isEmpty(value)){
            return "";
        }
        EsSecretSetting secretSetting = JsonUtil.jsonToObject(value,EsSecretSetting.class);

        if(!secretKey.equals(secretSetting.getSecretKey())){
            throw new ServiceException(GoodsErrorCode.E310.code(),"秘钥验证失败！");
        }
        String res = this.customWordsManager.deploy();
        try {
            return new String(res.getBytes(),"utf-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";

    }


}
