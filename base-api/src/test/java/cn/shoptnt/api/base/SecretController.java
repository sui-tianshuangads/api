package cn.shoptnt.api.base;


import cn.shoptnt.framework.security.model.TokenConstant;
import cn.shoptnt.framework.util.AESUtil;
import cn.shoptnt.mapper.member.MemberAddressMapper;
import cn.shoptnt.mapper.member.MemberMapper;
import cn.shoptnt.mapper.member.ShopDetailMapper;
import cn.shoptnt.mapper.trade.aftersale.AfterSaleChangeMapper;
import cn.shoptnt.mapper.trade.aftersale.AfterSaleServiceMapper;
import cn.shoptnt.mapper.trade.order.OrderMapper;
import cn.shoptnt.mapper.trade.order.TradeMapper;
import cn.shoptnt.model.aftersale.dos.AfterSaleChangeDO;
import cn.shoptnt.model.aftersale.dos.AfterSaleServiceDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberAddress;
import cn.shoptnt.model.shop.dos.ShopDetailDO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/secret")
public class SecretController {


    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private ShopDetailMapper shopDetailMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private TradeMapper tradeMapper;

    @Autowired
    private MemberAddressMapper memberAddressMapper;

    @Autowired
    private AfterSaleChangeMapper afterSaleChangeMapper;

    @Autowired
    private AfterSaleServiceMapper afterSaleServiceMapper;


    @GetMapping("/member")
    public String secretMember(){

        List<Member> list = memberMapper.selectList(null);
        list.forEach(m->{
            m.setMobile(AESUtil.decrypt(m.getMobile(), TokenConstant.SECRET));
            
            memberMapper.updateById(m);
        });

        return "会员成功";
    }

    @GetMapping("/shop")
    public String secretShop(){

        List<ShopDetailDO> list = shopDetailMapper.selectList(null);
        list.forEach(s->{
            s.setLinkPhone(AESUtil.decrypt(s.getLinkPhone(), TokenConstant.SECRET));
            s.setCompanyPhone(AESUtil.decrypt(s.getCompanyPhone(), TokenConstant.SECRET));
            s.setLegalId(AESUtil.decrypt(s.getLegalId(), TokenConstant.SECRET));
            s.setBankNumber(AESUtil.decrypt(s.getBankNumber(), TokenConstant.SECRET));
            shopDetailMapper.updateById(s);
        });
        return "店铺成功";
    }

    @GetMapping("/order")
    public String secretOrder(){

        List<OrderDO> list = orderMapper.selectList(null);
        list.forEach(o->{
            o.setShipMobile(AESUtil.decrypt(o.getShipMobile(), TokenConstant.SECRET));
            orderMapper.updateById(o);
        });

        List<TradeDO> list1 = tradeMapper.selectList(null);
        list1.forEach(o->{
            o.setConsigneeMobile(AESUtil.decrypt(o.getConsigneeMobile(), TokenConstant.SECRET));
            tradeMapper.updateById(o);
        });

        return "订单交易成功";
    }

    @GetMapping("/member-address")
    public String secretAddress(){

        List<MemberAddress> list = memberAddressMapper.selectList(null);
        list.forEach(o->{
            o.setMobile(AESUtil.decrypt(o.getMobile(), TokenConstant.SECRET));
            memberAddressMapper.updateById(o);
        });
        return "会员地址成功";
    }

    @GetMapping("/aftersale")
    public String secretAftersale(){

        List<AfterSaleChangeDO> list = afterSaleChangeMapper.selectList(null);
        list.forEach(o->{
            o.setShipMobile(AESUtil.decrypt(o.getShipMobile(), TokenConstant.SECRET));
            afterSaleChangeMapper.updateById(o);
        });


        List<AfterSaleServiceDO> list1 = afterSaleServiceMapper.selectList(null);
        list1.forEach(o->{
            o.setMobile(AESUtil.decrypt(o.getMobile(), TokenConstant.SECRET));
            afterSaleServiceMapper.updateById(o);
        });

        return "售后成功";
    }




}
