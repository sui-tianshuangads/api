/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyCatDO;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyCatManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 团购分类控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 16:08:03
 */
@RestController
@RequestMapping("/admin/promotion/group-buy-cats")
@Tag(name = "团购分类相关API")
@Validated
public class GroupbuyCatManagerController {

    @Autowired
    private GroupbuyCatManager groupbuyCatManager;


    @Operation(summary = "查询团购分类列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.groupbuyCatManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加团购分类")
    @PostMapping
    public GroupbuyCatDO add(@Valid @RequestBody GroupbuyCatDO groupbuyCat) {

        this.groupbuyCatManager.add(groupbuyCat);

        return groupbuyCat;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改团购分类")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public GroupbuyCatDO edit(@Valid @RequestBody GroupbuyCatDO groupbuyCat, @PathVariable Long id) {

        this.groupbuyCatManager.edit(groupbuyCat, id);

        return groupbuyCat;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除团购分类")
    @Parameters({
            @Parameter(name = "id", description = "要删除的团购分类主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.groupbuyCatManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个团购分类")
    @Parameters({
            @Parameter(name = "id", description = "要查询的团购分类主键", required = true,  in = ParameterIn.PATH)
    })
    public GroupbuyCatDO get(@PathVariable Long id) {
        GroupbuyCatDO groupbuyCat = this.groupbuyCatManager.getModel(id);
        return groupbuyCat;
    }

}
