/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.model.distribution.dos.BillTotalDO;
import cn.shoptnt.service.distribution.BillTotalManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 分销商总结算单
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午2:47
 */
@Tag(name = "分销商总结算单")
@RestController
@RequestMapping("/admin/distribution/bill/total")
public class BillTotalManagerController {

    @Autowired
    private BillTotalManager billTotalManager;

    @Parameters({
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
    })
    @GetMapping
    @Operation(summary = "结算单分页")
    public WebPage<BillTotalDO> page(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return billTotalManager.page(pageNo, pageSize);
    }


}
