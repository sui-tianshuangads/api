/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.distribution.dos.UpgradeLogDO;
import cn.shoptnt.service.distribution.UpgradeLogManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 升级日志
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午4:08
 */

@RestController
@RequestMapping("/admin/distribution/upgradelog")
@Tag(name = "升级日志")
public class UpgradeLogManagerController {

    @Autowired
    private UpgradeLogManager upgradeLogManager;

    @Operation(summary = "获取升级日志")
    @GetMapping
    @Parameters({
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "member_name", description = "会员名", in = ParameterIn.QUERY),
    })
    public WebPage<UpgradeLogDO> page(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String memberName) {
        return upgradeLogManager.page(pageNo, pageSize, memberName);
    }


}
