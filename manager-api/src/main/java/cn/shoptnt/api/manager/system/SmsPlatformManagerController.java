/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import cn.shoptnt.model.system.dos.SmsPlatformDO;
import cn.shoptnt.model.system.vo.SmsPlatformVO;
import cn.shoptnt.service.system.SmsPlatformManager;
import cn.shoptnt.framework.database.WebPage;




import javax.validation.constraints.NotEmpty;


/**
 * 短信网关表控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-23 11:31:05
 */
@RestController
@RequestMapping("/admin/systems/platforms")
@Tag(name = "短信网关相关API")
public class SmsPlatformManagerController {

    @Autowired
    private SmsPlatformManager smsPlatformManager;


    @Operation(summary = "查询短信网关列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize) {
        return this.smsPlatformManager.list(pageNo, pageSize);
    }


    @Operation(summary = "修改短信网关")
    @PutMapping(value = "/{bean}")
    @Parameters({
            @Parameter(name = "bean", description = "短信网关bean id", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sms_platform", description = "短信网关对象", required = true)
    })
    @Log(client = LogClient.admin,detail = "修改短信参数，${smsPlatformVO.name}",level = LogLevel.important)
    public SmsPlatformVO edit(@PathVariable String bean, @RequestBody @Parameter(hidden = true) SmsPlatformVO smsPlatformVO) {
        smsPlatformVO.setBean(bean);
        return this.smsPlatformManager.edit(smsPlatformVO);
    }


    @GetMapping(value = "/{bean}")
    @Operation(summary = "查询一个短信网关参数")
    @Parameters({
            @Parameter(name = "bean", description = "短信网关bean id", required = true,   in = ParameterIn.PATH)
    })
    public SmsPlatformVO getConfig(@PathVariable String bean) {
        return this.smsPlatformManager.getConfig(bean);
    }

    @Operation(summary = "开启某个短信网关")
    @PutMapping("/{bean}/open")
    @Parameter(name = "bean", description = "bean", required = true,   in = ParameterIn.PATH)
    public String open(@PathVariable String bean) {
        this.smsPlatformManager.openPlatform(bean);
        return null;
    }

}
