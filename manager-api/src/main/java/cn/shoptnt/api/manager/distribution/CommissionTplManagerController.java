/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.dos.CommissionTpl;
import cn.shoptnt.service.distribution.CommissionTplManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 模版控制器
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午3:15
 */
@RestController
@RequestMapping("/admin/distribution/commission-tpl")
@Tag(name = "模版")
public class CommissionTplManagerController {
    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private CommissionTplManager commissionTplManager;

    @Operation(summary = "添加模版")
    @PostMapping
    public CommissionTpl save(@Valid CommissionTpl commissionTpl) {
        try {
            return this.commissionTplManager.add(commissionTpl);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("添加模版异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @Operation(summary = "修改模版")
    @PutMapping(value = "/{tplId}")
    @Parameter(name = "tplId", description = "模版id", in = ParameterIn.PATH)
    public CommissionTpl saveEdit(@PathVariable @Parameter(hidden = true) Long tplId, @Valid CommissionTpl commissionTpl) {

        //如果要将默认模版中默认修改为不默认则提示
        if (commissionTpl.getIsDefault() == 0) {
            CommissionTpl com = this.commissionTplManager.getModel(tplId);
            if (com.getIsDefault() == 1) {
                throw new DistributionException(DistributionErrorCode.E1013.code(), DistributionErrorCode.E1013.des());
            }
        }
        //修改
        try {
            commissionTpl.setId(tplId);
            return this.commissionTplManager.edit(commissionTpl);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("修改模版失败", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }

    }


    @Operation(summary = "模板列表")
    @GetMapping
    @Parameters({
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
    })
    public WebPage<CommissionTpl> listJson(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.commissionTplManager.page(pageNo, pageSize);
    }


    @Operation(summary = "获取模板")
    @GetMapping(value = "/{tplId}")
    @Parameter(name = "tplId", description = "模版id", in = ParameterIn.PATH)
    public CommissionTpl getModel(@PathVariable Long tplId) {
        return this.commissionTplManager.getModel(tplId);
    }


    @Operation(summary = "删除模板")
    @DeleteMapping(value = "/{tplId}")
    @Parameter(name = "tplId", description = "模版id", in = ParameterIn.PATH)
    public void delete(@PathVariable Long tplId) {
        try {
            this.commissionTplManager.delete(tplId);

        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("删除模版失败", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }

    }
}
