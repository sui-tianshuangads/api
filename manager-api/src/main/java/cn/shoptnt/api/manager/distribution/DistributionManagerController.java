/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.vo.DistributionVO;
import cn.shoptnt.service.distribution.DistributionManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 后台分销商管理
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/25 上午10:55
 */
@RestController
@RequestMapping("/admin/distribution/member")
@Tag(name = "分销商")
public class DistributionManagerController {

    @Autowired
    private DistributionManager distributionManager;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Operation(summary = "分销商列表")
    @GetMapping
    @Parameters({
            @Parameter(name = "member_name", description = "会员名字", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
    })
    public WebPage<DistributionVO> page( @Parameter(hidden = true) Long pageNo,  @Parameter(hidden = true) Long pageSize,  @Parameter(hidden = true) String memberName) {

        try {
            return distributionManager.page(pageNo, pageSize, memberName);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            logger.error("获取模版异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    @Operation(summary = "修改模版")
    @PutMapping("/tpl")
    @Parameters({
            @Parameter(name = "member_id", description = "会员ID", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "tpl_id", description = "模版id", required = true, in = ParameterIn.QUERY),
    })
    public void changeTpl( @Parameter(hidden = true) Long memberId,  @Parameter(hidden = true) Long tplId) throws Exception {
        if (memberId == null || tplId == null) {
            throw new DistributionException(DistributionErrorCode.E1011.code(), DistributionErrorCode.E1011.des());
        }
        try {
            distributionManager.changeTpl(memberId, tplId);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("修改模版异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


}
