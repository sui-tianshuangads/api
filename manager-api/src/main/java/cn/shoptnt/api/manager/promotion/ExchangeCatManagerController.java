/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.model.promotion.exchange.dos.ExchangeCat;
import cn.shoptnt.service.promotion.exchange.ExchangeCatManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 积分分类控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-29 16:56:22
 */
@RestController
@RequestMapping("/admin/promotion/exchange-cats")
@Tag(name = "积分分类相关API")
@Validated
public class ExchangeCatManagerController {

    @Autowired
    private ExchangeCatManager exchangeCatManager;


    @Operation(summary = "查询某分类下的子分类列表")
    @Parameters({
            @Parameter(name = "parent_id", description = "父id，顶级为0", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{parent_id}/children")
    public List list(@Parameter(hidden = true) @PathVariable("parent_id") Long parentId) {
        return this.exchangeCatManager.list(parentId);
    }


    @Operation(summary = "添加积分兑换分类")
    @PostMapping
    public ExchangeCat add(@Valid ExchangeCat exchangeCat) {

        this.exchangeCatManager.add(exchangeCat);

        return exchangeCat;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改积分兑换分类")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public ExchangeCat edit(@Valid ExchangeCat exchangeCat, @PathVariable Long id) {

        this.exchangeCatManager.edit(exchangeCat, id);

        return exchangeCat;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除积分兑换分类")
    @Parameters({
            @Parameter(name = "id", description = "要删除的积分兑换分类主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.exchangeCatManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个积分兑换分类")
    @Parameters({
            @Parameter(name = "id", description = "要查询的积分兑换分类主键", required = true,  in = ParameterIn.PATH)
    })
    public ExchangeCat get(@PathVariable Long id) {

        ExchangeCat exchangeCat = this.exchangeCatManager.getModel(id);

        return exchangeCat;
    }

}
