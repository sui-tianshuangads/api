/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.IndustryStatisticManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 行业分析
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午6:47
 */

@RestController
@Tag(name = "统计 行业分析")
@RequestMapping("/admin/statistics/industry")
public class IndustryStatisticManagerController {

    @Autowired
    private IndustryStatisticManager industryAnalysisManager;

    @Operation(summary = "按分类统计下单量")
    @GetMapping(value = "/order/quantity")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart getOrderQuantity(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getOrderQuantity(searchCriteria);
    }

    @Operation(summary = "按分类统计下单商品数量")
    @GetMapping(value = "/goods/num")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart getGoodsNum(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getGoodsNum(searchCriteria);
    }

    @Operation(summary = "按分类统计下单金额")
    @GetMapping(value = "/order/money")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart getOrderMoney(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getOrderMoney(searchCriteria);
    }

    @Operation(summary = "概括总览")
    @GetMapping(value = "/overview")
    public WebPage getGeneralOverview(SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getGeneralOverview(searchCriteria);
    }
}
