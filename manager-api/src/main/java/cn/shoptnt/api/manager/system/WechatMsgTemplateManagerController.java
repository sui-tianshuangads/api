/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.WechatMsgTemplate;
import cn.shoptnt.service.system.WechatMsgTemplateManager;
import cn.shoptnt.framework.database.WebPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;

/**
 * 微信服务消息模板控制器
 *
 * @author fk
 * @version v7.1.4
 * @since vv7.1.0
 * 2019-06-14 16:42:35
 */
@RestController
@RequestMapping("/admin/systems/wechat-msg-tmp")
@Tag(name = "微信服务消息模板相关API")
public class WechatMsgTemplateManagerController {

    @Autowired
    private WechatMsgTemplateManager wechatMsgTemplateManager;


    @Operation(summary = "查询微信服务消息模板列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.wechatMsgTemplateManager.list(pageNo, pageSize);
    }

    @Operation(summary = "查询微信服务消息模板是否已经同步")
    @GetMapping("/sync")
    public boolean sync(){

        return wechatMsgTemplateManager.isSycn();
    }

    @Operation(summary = "同步微信服务消息模板")
    @PostMapping("/sync")
    public String syncMsgTmp(){

        wechatMsgTemplateManager.sycn();

        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个微信服务消息模板")
    @Parameters({
            @Parameter(name = "id", description = "要查询的微信服务消息模板主键", required = true,  in = ParameterIn.PATH)
    })
    public WechatMsgTemplate get(@PathVariable Long id) {

        WechatMsgTemplate wechatMsgTemplate = this.wechatMsgTemplateManager.getModel(id);

        return wechatMsgTemplate;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改微信服务消息模板")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public WechatMsgTemplate edit(@Valid WechatMsgTemplate wechatMsgTemplate, @PathVariable Long id) {

        this.wechatMsgTemplateManager.edit(wechatMsgTemplate, id);

        return wechatMsgTemplate;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除微信服务消息模板")
    @Parameters({
            @Parameter(name = "id", description = "要删除的微信服务消息模板主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.wechatMsgTemplateManager.delete(id);

        return "";
    }




}
