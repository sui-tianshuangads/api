/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.GoodsStatisticManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 商品统计控制器，后台
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-03-23 上午5:19
 */

@RestController
@Tag(name = "后台-》商品统计")
@RequestMapping("/admin/statistics/goods")
public class GoodsStatisticManagerController {

    @Autowired
    protected GoodsStatisticManager goodsStatisticsService;


    @Operation(summary = "价格销量统计")
    @GetMapping(value = "/price/sales")
    @Parameter(name = "prices", description = "价格区间", in = ParameterIn.QUERY)
    public SimpleChart getPriceSales(SearchCriteria searchCriteria, @RequestParam(required = false) Integer[] prices) {
        return this.goodsStatisticsService.getPriceSales(searchCriteria, prices);
    }

    @Operation(summary = "热卖商品按金额统计")
    @GetMapping(value = "/hot/money")
    public SimpleChart getHotSalesMoney(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesMoney(searchCriteria);
    }

    @Operation(summary = "热卖商品按金额统计")
    @GetMapping(value = "/hot/money/page")
    public WebPage getHotSalesMoneyPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesMoneyPage(searchCriteria);
    }

    @Operation(summary = "热卖商品按数量统计")
    @GetMapping(value = "/hot/num")
    public SimpleChart getHotSalesNum(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesNum(searchCriteria);
    }


    @Operation(summary = "热卖商品按数量统计")
    @GetMapping(value = "/hot/num/page")
    public WebPage getHotSalesNumPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesNumPage(searchCriteria);
    }

    @Operation(summary = "商品销售明细")
    @GetMapping(value = "/sale/details")
    @Parameters({
            @Parameter(name = "goods_name", description = "商品名字", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
    })
    public WebPage getSaleDetails(SearchCriteria searchCriteria, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long pageNo) {
        return this.goodsStatisticsService.getSaleDetails(searchCriteria, goodsName, pageSize, pageNo);
    }

    @Operation(summary = "商品收藏排行")
    @GetMapping(value = "/collect")
    public SimpleChart getGoodsCollect(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getGoodsCollect(searchCriteria);
    }

    @Operation(summary = "商品收藏排行")
    @GetMapping(value = "/collect/page")
    public WebPage getGoodsCollectPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getGoodsCollectPage(searchCriteria);
    }

}
