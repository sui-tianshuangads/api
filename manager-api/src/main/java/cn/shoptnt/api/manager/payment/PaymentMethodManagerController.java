/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.payment;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import cn.shoptnt.model.payment.dos.PaymentMethodDO;
import cn.shoptnt.model.payment.vo.PaymentPluginVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.payment.PaymentMethodManager;
import cn.shoptnt.framework.database.WebPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 支付方式表控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-04-11 16:06:57
 */
@RestController
@RequestMapping("/admin/payment/payment-methods")
@Tag(name = "支付方式表相关API")
public class PaymentMethodManagerController {

    @Autowired
    private PaymentMethodManager paymentMethodManager;


    @Operation(summary = "查询支付方式列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.paymentMethodManager.list(pageNo, pageSize);
    }


    @Operation(summary = "修改支付方式")
    @Parameters({
            @Parameter(name = "payment_plugin_id", description = "支付插件id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "payment_method", description = "支付方式对象", required = true)
    })
    @PutMapping("/{payment_plugin_id}")
    @Log(client = LogClient.admin, detail = "修改支付参数，${paymentMethod.methodName}", level = LogLevel.important)
    public PaymentMethodDO add(@Valid @RequestBody @Parameter(hidden = true) PaymentPluginVO paymentMethod, @PathVariable("payment_plugin_id") String paymentPluginId) {

        return this.paymentMethodManager.add(paymentMethod, paymentPluginId);
    }

    @GetMapping(value = "/{plugin_id}")
    @Operation(summary = "查询一个支付方式")
    @Parameters({
            @Parameter(name = "plugin_id", description = "要查询的支付插件id", required = true, in = ParameterIn.PATH)
    })
    public PaymentPluginVO get(@PathVariable("plugin_id") String pluginId) {

        return this.paymentMethodManager.getByPlugin(pluginId);
    }

}
