package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.model.base.message.PromotionScriptInitMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 促销活动脚本初始化相关API
 * @author dmy
 * 2022-09-23
 */
@RestController
@RequestMapping("/admin/promotion/script-init")
@Tag(name = "促销活动脚本初始化相关API")
@Validated
public class PromotionScriptInitManagerController {

    @Autowired
    private DirectMessageSender messageSender;

    @PostMapping
    @Operation(summary = "促销活动脚本初始化")
    public void scriptInit() {
        /** 发送促销活动脚本初始化消息 */
        messageSender.send(new PromotionScriptInitMessage());
    }
}
