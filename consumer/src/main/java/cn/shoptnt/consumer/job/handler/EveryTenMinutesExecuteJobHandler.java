/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.job.handler;

import cn.shoptnt.job.dispatcher.EveryTenMinutesDispatcher;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @Description: 每十分钟执行定时任务
 * @Author: gy
 * @Date: 2020/6/10 0010 9:50
 */
@Component
public class EveryTenMinutesExecuteJobHandler {

    @Autowired
    private EveryTenMinutesDispatcher dispatcher;

    @XxlJob("everyTenMinutesExecuteJobHandler")
    public ReturnT<String> execute(String param) {
        Boolean dispatch = dispatcher.dispatch();
        if (!dispatch) {
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}
