/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.shop.ShopChangeDispatcher;
import cn.shoptnt.model.shop.vo.ShopChangeMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 店铺变更消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:32:08
 */
@Component
public class ShopChangeReceiver {

    @Autowired
    private ShopChangeDispatcher dispatcher;

    /**
     * 店铺变更
     *
     * @param shopChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_CHANGE + "_ROUTING"),
            exchange = @Exchange(value = AmqpExchange.SHOP_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void shopChange(ShopChangeMsg shopChangeMsg) {
        dispatcher.dispatch(shopChangeMsg);
    }

}
