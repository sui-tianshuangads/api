/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.GoodsIndexInitDispatcher;
import cn.shoptnt.model.base.message.IndexCreateMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商品索引
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:30:12
 */
@Component
public class GoodsIndexInitReceiver {

    @Autowired
    private GoodsIndexInitDispatcher dispatcher;

    /**
     * 初始化商品索引
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.INDEX_CREATE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.INDEX_CREATE, type = ExchangeTypes.FANOUT)
    ))
    public void initGoodsIndex(IndexCreateMessage message) {
        dispatcher.dispatch(message);
    }

}
