/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.shop.ShipTemplateChangeDispatcher;
import cn.shoptnt.model.goods.vo.ShipTemplateMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 运费模板变化消息
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2019年9月16日 上午10:29:42
 */
@Component
public class ShipTemplateChangeReceiver {

    @Autowired
    private ShipTemplateChangeDispatcher dispatcher;


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHIP_TEMPLATE_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SHIP_TEMPLATE_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void categoryChange(ShipTemplateMsg shipTemplateMsg) {
        dispatcher.dispatch(shipTemplateMsg);
    }
}
