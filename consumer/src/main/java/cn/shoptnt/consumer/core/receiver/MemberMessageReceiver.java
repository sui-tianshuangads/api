/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.MemberMessageDispatcher;
import cn.shoptnt.model.base.message.MemberSiteMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 站内消息
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:13
 */
@Component
public class MemberMessageReceiver {

    @Autowired
    private MemberMessageDispatcher dispatcher;

    /**
     * 站内消息
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_MESSAGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_MESSAGE, type = ExchangeTypes.FANOUT)
    ))
    public void memberMessage(MemberSiteMessage message) {
        dispatcher.dispatch(message);
    }
}
