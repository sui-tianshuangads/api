/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.SearchKeywordDispatcher;
import cn.shoptnt.model.base.message.GoodsSearchMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: 关键字搜索历史消息
 * @date 2019/5/27 12:00
 * @since v7.0
 */
@Component
public class SearchKeywordReceiver {

    @Autowired
    private SearchKeywordDispatcher dispatcher;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SEARCH_KEYWORDS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SEARCH_KEYWORDS, type = ExchangeTypes.FANOUT)
    ))
    public void refund(GoodsSearchMessage message) {
        dispatcher.dispatch(message);
    }
}
