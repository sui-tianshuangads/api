/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.trade;

import cn.shoptnt.model.trade.order.vo.InvoiceVO;
import cn.shoptnt.service.trade.order.OrderQueryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 发货单控制器
 *
 * @author zh
 * @version v7.0
 * @date 2019/11/5 10:31 AM
 * @since v7.0
 */
@Tag(name = "发货单API")
@RestController
@RequestMapping("/seller/trade/invoice")
@Validated
public class InvoiceSellerController {


    @Autowired
    private OrderQueryManager orderQueryManager;


    @Operation(summary = "查询发货单信息")
    @Parameters({
            @Parameter(name = "order_id", description = "订单Id", required = true,  in = ParameterIn.QUERY),
    })
    @GetMapping
    public InvoiceVO create(@RequestParam(value = "order_id")  Long orderId) {
        return this.orderQueryManager.getInvoice(orderId);
    }


}
