/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.errorcode.StatisticsErrorCode;
import cn.shoptnt.model.statistics.exception.StatisticsException;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.GoodsFrontStatisticsManager;
import cn.shoptnt.framework.context.user.UserContext;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 商家中心，商品分析
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年4月18日上午11:57:48
 */
@Tag(name = "商家统计 商品分析")
@RestController
@RequestMapping("/seller/statistics/goods")
public class GoodsStatisticsSellerController {

    @Autowired
    private GoodsFrontStatisticsManager goodsFrontStatisticsManager;

    @Operation(summary = "商品详情，获取近30天销售数据")
    @GetMapping(value = "/goods_detail")
    @Parameters({@Parameter(name = "page_no", description = "当前页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页面大小", in = ParameterIn.QUERY),
            @Parameter(name = "category_id", description = "商品分类id", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY)})
    public WebPage getGoodsSalesDetail(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long categoryId, @Parameter(hidden = true) String goodsName) {

        if (null == categoryId) {
            throw new StatisticsException(StatisticsErrorCode.E801.code(), "商品分类id不可为空");
        }
        return this.goodsFrontStatisticsManager.getGoodsDetail(pageNo, pageSize, categoryId, goodsName);
    }

    @Operation(summary = "价格销量")
    @GetMapping(value = "/price_sales")
    @Parameters({
            @Parameter(name = "sections", description = "价格区间，不传有默认值", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "category_id", description = "商品分类id", in = ParameterIn.QUERY)
    })
    public SimpleChart getGoodsPriceSales(@Parameter(hidden = true) @Valid SearchCriteria searchCriteria, @Parameter(hidden = true) @RequestParam(value = "sections") ArrayList<Integer> sections) {

        // 如果价格空间为空，则添加默认值
        if (null == sections || sections.size() == 0) {
            sections = new ArrayList<>();
            sections.add(0);
            sections.add(500);
            sections.add(500);
            sections.add(1000);
            sections.add(1000);
            sections.add(1500);
            sections.add(1500);
            sections.add(2000);
        }

        // 去除null值
        sections.removeIf(Objects::isNull);
        if (sections.size() < 2) {
            throw new StatisticsException(StatisticsErrorCode.E801.code(), "应至少上传两个数字，才可构成价格区间");
        }
        searchCriteria.setSellerId(UserContext.getSeller().getSellerId());
        return this.goodsFrontStatisticsManager.getGoodsPriceSales(sections, searchCriteria);
    }

    @Operation(summary = "下单金额排行前30商品，分页数据")
    @GetMapping(value = "/order_price_page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    public WebPage getGoodsOrderPriceTopPage(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        // 排名名次 默认30
        Integer topNum = 30;
        // 获取数据
        return this.goodsFrontStatisticsManager.getGoodsOrderPriceTopPage(topNum, searchCriteria);
    }

    @Operation(summary = "下单商品数量排行前30，分页数据")
    @GetMapping(value = "/order_num_page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    public WebPage getGoodsNumTop(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.goodsFrontStatisticsManager.getGoodsNumTopPage(30, searchCriteria);
    }

    @Operation(summary = "下单金额排行前30商品，图表数据")
    @GetMapping(value = "/order_price")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    public SimpleChart getGoodsOrderPriceTopChart(@Parameter(hidden = true) SearchCriteria searchCriteria) {

        // 排名名次 默认30
        Integer topNum = 30;

        // 2.获取数据
        return this.goodsFrontStatisticsManager.getGoodsOrderPriceTop(topNum, searchCriteria);
    }

    @Operation(summary = "下单商品数量排行前30，图表数据")
    @GetMapping(value = "/order_num")
    @Parameters({
            @Parameter(name = "cycle_type", description = "周期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY)})
    public SimpleChart getGoodsNumTopChart(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        // 获取数据
        return this.goodsFrontStatisticsManager.getGoodsNumTop(30, searchCriteria);
    }

}
