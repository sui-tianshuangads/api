/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.trade;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dto.OrderDetailQueryParam;
import cn.shoptnt.model.trade.order.dto.OrderQueryParam;
import cn.shoptnt.model.trade.order.enums.PaymentTypeEnum;
import cn.shoptnt.model.trade.order.vo.*;
import cn.shoptnt.service.trade.order.OrderOperateManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 商家订单控制器
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */

@Tag(name = "商家订单API")
@RestController
@RequestMapping("/seller/trade/orders")
@Validated
public class OrderSellerController {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;

    @Operation(summary = "查询会员订单列表")
    @Parameters({
            @Parameter(name = "keywords", description = "关键字", in = ParameterIn.QUERY),
            @Parameter(name = "order_sn", description = "订单编号", in = ParameterIn.QUERY),
            @Parameter(name = "buyer_name", description = "买家姓名", in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY,
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @Parameter(name = "page_no", description = "页数", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数", in = ParameterIn.QUERY),
            @Parameter(name = "client_type", description = "订单来源", in = ParameterIn.QUERY)
    })
    @GetMapping()
    public WebPage<OrderLineVO> list(@Valid OrderQueryParam param,
                                     @Parameter(hidden = true) String orderStatus,
                                     @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        Seller seller = UserContext.getSeller();
        //名单状态字段orderStatus用于tag查询
        param.setOrderStatus(null);
        param.setTag(orderStatus);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setSellerId(seller.getSellerId());

        WebPage<OrderLineVO> page = this.orderQueryManager.list(param);
        return page;
    }


    @Operation(summary = "查询单个订单明细")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{order_sn}")
    public OrderDetailVO get(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {
        Seller seller = UserContext.getSeller();
        OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
        queryParam.setSellerId(seller.getSellerId());
        OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, queryParam);
        // 校验订单权限
        if (!detailVO.getSellerId().equals(seller.getSellerId())) {
            throw new ServiceException(TradeErrorCode.E460.code(), "操作订单无权限");
        }

        //在线支付的订单商家不允许确认收款
        if (PaymentTypeEnum.ONLINE.value().equals(detailVO.getPaymentType())) {
            detailVO.getOrderOperateAllowableVO().setAllowPay(false);
        }


        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(orderSn));
        }

        return detailVO;
    }


    @Operation(summary = "查询订单状态的数量")
    @GetMapping(value = "/status-num")
    public OrderStatusNumVO getStatusNum() {
        Seller seller = UserContext.getSeller();
        OrderStatusNumVO orderStatusNumVO = this.orderQueryManager.getOrderStatusNum(null, seller.getSellerId());
        return orderStatusNumVO;
    }


    @Operation(summary = "订单发货")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单sn", required = true, in = ParameterIn.PATH),
            @Parameter(name = "ship_no", description = "发货单号", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "logi_id", description = "物流公司id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "logi_name", description = "物流公司名称", required = true, in = ParameterIn.QUERY),
    })
    @ResponseBody
    @PostMapping(value = "/{order_sn}/delivery")
    @Log(client = LogClient.seller, detail = "订单发货，订单号：${orderSn}")
    public String ship(@Parameter(hidden = true) @NotNull(message = "必须指定订单编号") @PathVariable(name = "order_sn") String orderSn,
                       @Parameter(hidden = true) @NotNull(message = "必须输入发货单号") @Length(max = 20, message = "物流单号不正确") String shipNo,
                       @Parameter(hidden = true) @NotNull(message = "必须选择物流公司") Long logiId,
                       @Parameter(hidden = true) String logiName) {

        Seller seller = UserContext.getSeller();
        DeliveryVO delivery = new DeliveryVO();
        delivery.setDeliveryNo(shipNo);
        delivery.setOrderSn(orderSn);
        delivery.setLogiId(logiId);
        delivery.setLogiName(logiName);
        delivery.setOperator("店铺:" + seller.getSellerName());
        orderOperateManager.ship(delivery, OrderPermission.seller);

        return "";
    }


    @Operation(summary = "商家修改收货人地址")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单sn", required = true, in = ParameterIn.PATH),
            @Parameter(name = "ship_name", description = "收货人姓名", in = ParameterIn.QUERY),
            @Parameter(name = "remark", description = "订单备注", in = ParameterIn.QUERY),
            @Parameter(name = "ship_addr", description = "收货地址", in = ParameterIn.QUERY),
            @Parameter(name = "ship_mobile", description = "收货人手机号", in = ParameterIn.QUERY),
            @Parameter(name = "ship_tel", description = "收货人电话", in = ParameterIn.QUERY),
            @Parameter(name = "receive_time", description = "送货时间", in = ParameterIn.QUERY),
            @Parameter(name = "region", description = "地区id", in = ParameterIn.QUERY),
    })
    @PutMapping(value = "/{order_sn}/address")
    public OrderConsigneeVO updateOrderConsignee(@Parameter(hidden = true) @PathVariable(name = "order_sn") String orderSn,
                                                 @Parameter(hidden = true) String shipName, @Parameter(hidden = true) String remark,
                                                 @Parameter(hidden = true) String shipAddr, @Parameter(hidden = true) String shipMobile,
                                                 @Parameter(hidden = true) String shipTel, @Parameter(hidden = true) String receiveTime,
                                                 @RegionFormat Region region) {

        OrderConsigneeVO orderConsignee = new OrderConsigneeVO();
        orderConsignee.setOrderSn(orderSn);
        orderConsignee.setShipName(shipName);
        orderConsignee.setRemark(remark);
        orderConsignee.setShipAddr(shipAddr);
        orderConsignee.setShipMobile(shipMobile);
        orderConsignee.setShipTel(shipTel);
        orderConsignee.setReceiveTime(receiveTime);
        orderConsignee.setRegion(region);
        orderConsignee = this.orderOperateManager.updateOrderConsignee(orderConsignee);
        return orderConsignee;
    }


    @Operation(summary = "商家修改订单价格")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单sn", required = true, in = ParameterIn.PATH),
            @Parameter(name = "order_price", description = "订单价格", required = true, in = ParameterIn.QUERY),
    })
    @Log(client = LogClient.seller, detail = "编辑发货信息，订单号：${orderSn}", level = LogLevel.important)
    @PutMapping(value = "/{order_sn}/price")
    public String updateOrderPrice(@Parameter(hidden = true) @PathVariable(name = "order_sn") String orderSn,
                                   @Parameter(hidden = true) @NotNull(message = "修改后价格不能为空") @DecimalMin(value = "0.01", message = "最低金额为0.01") Double orderPrice) {
        this.orderOperateManager.updateOrderPrice(orderSn, orderPrice);
        return "";
    }


    @Operation(summary = "确认收款")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "pay_price", description = "付款金额", in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/{order_sn}/pay")
    public String payOrder(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn, @Parameter(hidden = true) Double payPrice) {
        this.orderOperateManager.payOrder(orderSn, payPrice, "", OrderPermission.seller);
        return "";
    }


    @Operation(summary = "订单流程图数据")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单sn", required = true, in = ParameterIn.PATH),
    })
    @GetMapping(value = "/{order_sn}/flow")
    public List<OrderFlowNode> getOrderStatusFlow(@Parameter(hidden = true) @PathVariable(name = "order_sn") String orderSn) {
        List<OrderFlowNode> orderFlowList = this.orderQueryManager.getOrderFlow(orderSn);
        return orderFlowList;
    }


    @Operation(summary = "导出订单列表")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", in = ParameterIn.QUERY),
            @Parameter(name = "ship_name", description = "收货人", in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "buyer_name", description = "买家名字", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY,
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @Parameter(name = "payment_type", description = "支付方式", in = ParameterIn.QUERY,
                    example = "ONLINE:在线支付,COD:货到付款"),
            @Parameter(name = "client_type", description = "订单来源", in = ParameterIn.QUERY,
                    example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping("/export")
    public List<OrderLineVO> export(@Parameter(hidden = true) String orderSn, @Parameter(hidden = true) String shipName, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String buyerName,
                                    @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime, @Parameter(hidden = true) String orderStatus,
                                    @Parameter(hidden = true) String paymentType, @Parameter(hidden = true) String clientType) {

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setTag(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setSellerId(UserContext.getSeller().getSellerId());
        param.setPaymentType(paymentType);
        param.setClientType(clientType);
        List<OrderLineVO> lineList = this.orderQueryManager.export(param);
        return lineList;
    }

    @Operation(summary = "取消订单")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "reason", description = "取消原因", required = true, in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/{order_sn}/cancel")
    public String cancel(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn, String reason) {

        CancelVO cancelVO = new CancelVO();
        cancelVO.setOperator("商家取消");
        cancelVO.setOrderSn(orderSn);
        cancelVO.setReason(reason);
        orderOperateManager.cancel(cancelVO, OrderPermission.seller);

        return "";
    }
}
