/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.model.goods.dos.TagsDO;
import cn.shoptnt.service.goods.TagsManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 商品标签控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@RestController
@RequestMapping("/seller/goods/tags")
@Tag(name = "商品标签相关API")
public class TagsSellerController {

    @Autowired
    private TagsManager tagsManager;


    @Operation(summary = "查询商品标签列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.tagsManager.list(pageNo, pageSize);
    }

    @Operation(summary = "查询某标签下的商品")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "tag_id", description = "标签id", required = true, in = ParameterIn.PATH)
    })
    @GetMapping("/{tag_id}/goods")
    public WebPage listGoods(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("tag_id") Long tagId) {

        return this.tagsManager.queryTagGoods(tagId, pageNo, pageSize);
    }

    @Operation(summary = "保存某标签下的商品")
    @Parameters({
            @Parameter(name = "tag_id", description = "标签id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "goods_ids", description = "要保存的商品id", required = true, in = ParameterIn.PATH),
    })
    @PutMapping("/{tag_id}/goods/{goods_ids}")
    public String saveGoods(@PathVariable("tag_id") Long tagId, @PathVariable("goods_ids") Long[] goodsIds) {

        this.tagsManager.saveTagGoods(tagId, goodsIds);

        return null;
    }


}
