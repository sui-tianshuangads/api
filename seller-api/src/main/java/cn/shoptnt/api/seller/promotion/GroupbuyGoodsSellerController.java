/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyGoodsDO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyGoodsVO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyQueryParam;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyActiveManager;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyGoodsManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 团购商品控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 16:57:26
 */
@RestController
@RequestMapping("/seller/promotion/group-buy-goods")
@Tag(name = "团购商品相关API")
@Validated
public class GroupbuyGoodsSellerController {

    @Autowired
    private GroupbuyGoodsManager groupbuyGoodsManager;

    @Autowired
    private GroupbuyActiveManager groupbuyActiveManager;

    @Operation(summary = "查询团购商品列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "act_name", description = "团购活动名称", in = ParameterIn.QUERY),
            @Parameter(name = "gb_name", description = "团购名称", in = ParameterIn.QUERY),
            @Parameter(name = "gb_status", description = "审核状态 0:待审核,1:审核通过,2:未通过审核", in = ParameterIn.QUERY),
            @Parameter(name = "act_status", description = "活动状态 NOT_STARTED:未开始,STARTED:进行中,OVER:已结束", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String actName,
                        @Parameter(hidden = true) String gbName, @Parameter(hidden = true) Integer gbStatus, @Parameter(hidden = true) String actStatus) {
        Seller seller = UserContext.getSeller();
        GroupbuyQueryParam param = new GroupbuyQueryParam();
        param.setSellerId(seller.getSellerId());
        param.setPage(pageNo);
        param.setPageSize(pageSize);
        param.setGoodsName(goodsName);
        param.setActName(actName);
        param.setGbName(gbName);
        param.setGbStatus(gbStatus);
        param.setActStatus(actStatus);
        param.setClientType("SELLER");
        return this.groupbuyGoodsManager.listPage(param);
    }


    @Operation(summary = "添加团购商品")
    @Parameter(name = "groupbuyGoods", description = "团购商品信息", required = true)
    @PostMapping
    public GroupbuyGoodsDO add(@Valid @RequestBody GroupbuyGoodsDO groupbuyGoods) {
        //团购活动id
        Long actId = groupbuyGoods.getActId();
        GroupbuyActiveDO model = groupbuyActiveManager.getModel(actId);
        // 判断活动截止时间和当前时间
        if (model.getJoinEndTime() < DateUtil.getDateline()) {
            throw new ServiceException(PromotionErrorCode.E408.code(), "团购活动已过报名截止时间，无法报名");
        }
        groupbuyGoods.setAddTime(DateUtil.getDateline());
        this.verifyParam(groupbuyGoods);
        Seller seller = UserContext.getSeller();
        groupbuyGoods.setSellerId(seller.getSellerId());
        groupbuyGoods.setSellerName(seller.getSellerName());
        groupbuyGoods.setBuyNum(0);
        groupbuyGoods.setViewNum(0);
        this.groupbuyGoodsManager.add(groupbuyGoods);

        return groupbuyGoods;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改团购商品")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true, in = ParameterIn.PATH)
    })
    public GroupbuyGoodsDO edit(@Valid @RequestBody GroupbuyGoodsDO groupbuyGoods, @PathVariable Long id) {

        this.verifyParam(groupbuyGoods);
        this.groupbuyGoodsManager.verifyAuth(id);
        this.groupbuyGoodsManager.edit(groupbuyGoods, id);

        return groupbuyGoods;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除团购商品")
    @Parameters({
            @Parameter(name = "id", description = "要删除的团购商品主键", required = true,in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.groupbuyGoodsManager.verifyAuth(id);
        this.groupbuyGoodsManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个团购商品")
    @Parameters({
            @Parameter(name = "id", description = "要查询的团购商品主键", required = true, in = ParameterIn.PATH)
    })
    public GroupbuyGoodsVO get(@PathVariable Long id) {
        GroupbuyGoodsVO groupbuyGoods = this.groupbuyGoodsManager.getModelAndQuantity(id);
        Seller seller = UserContext.getSeller();
        if (groupbuyGoods == null || !groupbuyGoods.getSellerId().equals(seller.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }
        return groupbuyGoods;
    }


    @Operation(summary = "查询可以参与的团购活动列表")
    @GetMapping(value = "/active")
    public List<GroupbuyActiveDO> listActive() {

        return this.groupbuyActiveManager.getActiveList();
    }


    /**
     * 验证参数
     *
     * @param goodsDO
     */
    private void verifyParam(GroupbuyGoodsDO goodsDO) {

        String gbName = goodsDO.getGbName();
        if (!StringUtil.validMaxLen(gbName, 30)) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "团购名称字数超限");
        }

        int maxValue = 999999999;
        if (goodsDO.getVisualNum() != null && goodsDO.getVisualNum().intValue() > maxValue) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "虚拟购买数量超出上限");
        }
    }

}
