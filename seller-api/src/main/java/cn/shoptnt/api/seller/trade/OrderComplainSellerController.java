/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.trade;

import cn.shoptnt.model.trade.complain.dos.OrderComplain;
import cn.shoptnt.model.trade.complain.dos.OrderComplainCommunication;
import cn.shoptnt.model.trade.complain.dto.ComplainQueryParam;
import cn.shoptnt.model.trade.complain.vo.OrderComplainVO;
import cn.shoptnt.service.trade.complain.OrderComplainCommunicationManager;
import cn.shoptnt.service.trade.complain.OrderComplainManager;
import cn.shoptnt.model.trade.order.vo.OrderFlowNode;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Seller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;

/**
 * 交易投诉表控制器
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
@RestController
@RequestMapping("/seller/trade/order-complains")
@Tag(name = "交易投诉表相关API")
public class OrderComplainSellerController {

    @Autowired
    private OrderComplainManager orderComplainManager;

    @Autowired
    private OrderComplainCommunicationManager orderComplainCommunicationManager;


    @Operation(summary = "查询交易投诉表列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, ComplainQueryParam param) {

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        Seller seller = UserContext.getSeller();
        param.setSellerId(seller.getSellerId());

        return this.orderComplainManager.list(param);
    }


    @PutMapping(value = "/{id}/appeal")
    @Operation(summary = "商家申诉")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "appeal_content", description = "申诉内容", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "images", description = "申诉图片，多图逗号分隔", required = true, in = ParameterIn.QUERY),
    })
    public OrderComplain auth(@PathVariable Long id,@Parameter(hidden = true) String appealContent,@Parameter(hidden = true) @RequestParam(value = "images",required = false)String[] images) {

        return this.orderComplainManager.appeal(id,appealContent,images);
    }


    @PutMapping(value = "/{id}/to-arbitration")
    @Operation(summary = "对话中提交仲裁，随时可以提交仲裁")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
    })
    public OrderComplain complete(@PathVariable Long id) {

        return this.orderComplainManager.arbitrate(id);
    }


    @PutMapping(value = "/{id}/communication")
    @Operation(summary = "提交对话")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "content", description = "对话内容", required = true,   in = ParameterIn.QUERY),
    })
    public OrderComplainCommunication communicate(@PathVariable Long id,String content) {

        Seller seller = UserContext.getSeller();
        OrderComplainCommunication communication = new OrderComplainCommunication(id,content,"卖家",seller.getSellerName(),seller.getSellerId());

        return this.orderComplainCommunicationManager.add(communication);
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个交易投诉")
    @Parameters({
            @Parameter(name = "id", description = "要查询的交易投诉表主键", required = true,  in = ParameterIn.PATH)
    })
    public OrderComplainVO get(@PathVariable Long id) {

        OrderComplainVO orderComplain = this.orderComplainManager.getModelAndCommunication(id);

        return orderComplain;
    }

    @GetMapping(value = "/{id}/flow")
    @Operation(summary = "查询一个交易投诉的流程图")
    @Parameters({
            @Parameter(name = "id", description = "要查询的交易投诉表主键", required = true,  in = ParameterIn.PATH)
    })
    public List<OrderFlowNode> getFlow(@PathVariable Long id) {

        return this.orderComplainManager.getComplainFlow(id);
    }



}
