/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.passport;

import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.model.member.vo.SellerInfoVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.service.shop.ClerkManager;
import cn.shoptnt.framework.ShopTntConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.NotEmpty;


/**
 * 会员登录注册API
 *
 * @author zh
 * @version v7.0
 * @since v7.0
 * 2018年3月23日 上午10:12:12
 */
@RestController
@RequestMapping("/seller/login")
@Tag(name = "商家登录API")
@Validated
public class PassportLoginSellerController {

    @Autowired
    private PassportManager passportManager;
    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private ClerkManager clerkManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private ShopTntConfig shoptntConfig;

    @PostMapping(value = "/smscode/{mobile}")
    @Operation(summary = "发送验证码")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH)
    })
    public String sendSmsCode(@PathVariable("mobile") String mobile) {

        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        passportManager.sendLoginSmsCode(mobile);
        return shoptntConfig.getSmscodeTimout() / 60 + "";
    }

    @GetMapping()
    @Operation(summary = "用户名（手机号）/密码登录API")
    @Parameters({
            @Parameter(name = "username", description = "用户名", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY)
    })
    public SellerInfoVO login(@NotEmpty(message = "用户名不能为空") String username, @NotEmpty(message = "密码不能为空") String password) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();
        return clerkManager.login(username, password);
    }

    @GetMapping("/login/{mobile}")
    @Operation(summary = "手机号码登录API")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sms_code", description = "手机验证码", required = true,   in = ParameterIn.QUERY)
    })
    @Log(client = LogClient.seller,detail = "${mobile}登陆")
    public MemberVO mobileLogin(@PathVariable String mobile, @Parameter(hidden = true) @NotEmpty(message = "短信验证码不能为空") String smsCode) {
        return memberManager.mobileLogin(mobile,smsCode, 2);

    }
}
