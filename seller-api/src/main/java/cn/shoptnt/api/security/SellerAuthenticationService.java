/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.security;

import cn.shoptnt.framework.auth.AuthUser;
import cn.shoptnt.framework.security.impl.AbstractAuthenticationService;
import cn.shoptnt.framework.security.model.Clerk;
import cn.shoptnt.framework.security.model.Role;
import cn.shoptnt.framework.security.model.User;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.framework.util.TokenKeyGenerate;
import org.springframework.stereotype.Component;

/**
 * seller 鉴权管理
 * v2.0: 重构鉴权机制
 * Created by zh on 2018/3/12.
 *
 * @author zh
 * @version 2.0
 * @since 7.0.0
 * 2018/3/12
 */
@Component
public class SellerAuthenticationService extends AbstractAuthenticationService {

    /**
     * 将token解析为Clerk
     *
     * @param token
     * @return
     */
    @Override
    protected AuthUser parseToken(String token) {
        AuthUser authUser = tokenManager.parse(Clerk.class, token);
        User user = (User) authUser;
        checkUserDisable(Role.CLERK, user.getUid());
        return authUser;
    }

}
