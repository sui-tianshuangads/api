package cn.shoptnt.framework.validation.annotation;

import cn.shoptnt.framework.validation.impl.RichTextSafeDomainValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author LiuXT
 * @Date 2022.06.13
 * <p>
 * 自定义注解 - 校验白名单域名（富文本）
 * @Documented：指定被标注的注解会包含在javadoc中
 * @Retention： 指定注解的生命周期
 *      RetentionPolicy.SOURCE  : 在编译阶段丢弃,这些注解在编译结束之后就不再有任何意义,不会写入字节码
 *      RetentionPolicy.CLASS   : 在类加载的时候丢弃,在字节码文件的处理中有用
 *      RetentionPolicy.RUNTIME : 始终不会丢弃,运行期也保留该注解,可使用反射机制读取该注解的信息(自定义注解常用)
 * @Target：指定注解使用的目标范围 ElementType.METHOD : 用于描述方法
 *      ElementType.FIELD :成员变量、对象、属性（包括enum实例）
 *      ElementType.LOCAL_VARIABLE : 用于描述局部变量
 *      ElementType.ANNOTATION_TYPE : 用于描述参数
 *      ElementType.PARAMETER : 用于描述参数
 *      ElementType.CONSTRUCTOR : 用于描述构造器
 *      ElementType.TYPE : 用于描述类、接口(包括注解类型) 或enum声明
 *      ElementType.PACKAGE : 用于描述包
 * @Constraint：指定校验逻辑的具体实现
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Constraint(validatedBy = {RichTextSafeDomainValidator.class})
public @interface RichTextSafeDomain {
    String message() default "富文本参数涉及非法域名";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
