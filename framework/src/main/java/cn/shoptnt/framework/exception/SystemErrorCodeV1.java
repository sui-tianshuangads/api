/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.exception;

/**
 * 系统级别异常码
 *
 * @author 妙贤
 * @version v1.0.0
 * @since v1.0.0
 * 2017年3月27日 下午6:56:35
 */
public class SystemErrorCodeV1 {

    /**
     * 无权限异常
     */
    public final static String NO_PERMISSION = "002";
    /**
     * 资源未能找到
     */
    public final static String RESOURCE_NOT_FOUND = "003";
    /**
     * 错误的请求参数
     */
    public final static String INVALID_REQUEST_PARAMETER = "004";
    /**
     * 错误的配置参数
     */
    public final static String INVALID_CONFIG_PARAMETER = "005";
    /**
     * 错误的配置参数
     */
    public final static String INVALID_COTENT = "006";

    /**
     * 空指针
     */
    public final static String NULL_POINTER = "007";

    /**
     * 数据不安全：数据已经被篡改
     */
    public final static String NOT_SAFE = "008";

}
