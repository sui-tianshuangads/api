package cn.shoptnt.framework.util;

import org.springframework.boot.system.ApplicationHome;

import java.io.File;

/**
 * 路径工具类
 * 提供一下基础路径的获取：项目根路径、索引存储路径、图片存啥路径
 * @author kingapex
 * @version 1.0
 * @data 2022/10/24 11:28
 **/
public class PathUtil {


    /**
     * 获取工程根目录
     * 如果是基于jar运行则是jar所在目录
     * 如果是基于class运行，则是工程目录
     * @return
     */
    public static String getProjectHome() {
        ApplicationHome home = new ApplicationHome(PathUtil.class);

        File jarFile = home.getSource();
        String aPath = jarFile.getAbsolutePath();

        File projectFile= null;

        // 如果基于jar运行，则以jar所在目录为根
        if (aPath.endsWith(".jar")) {
            projectFile = jarFile.getParentFile();
        } else {
            //如果基于类运行，则以工程根目录为根
            //获得的的路径是/xxx/api/framework/target/class
            //需要得到/xxx，所以：      /target         /framework      /api            /xxx
            projectFile = jarFile.getParentFile().getParentFile().getParentFile().getParentFile();;
        }
        return projectFile.getAbsolutePath();
    }


    /**
     * 获取存储根目录，默认是/shoptnt
     * @return
     */
    public static String getStorageRoot() {
        String projectHome = getProjectHome();
        String storageRoot = projectHome + File.separator + "files";
        return storageRoot;
    }


    /**
     * 获取索引存储根目录
     * @return
     */
    public static String getIndexRoot() {
        String storageRoot =getStorageRoot();
        String indexRoot = storageRoot + File.separator + "index";
        return indexRoot;
    }

    /**
     * 获取图片存储根路径
     * @return
     */
    public static String getImageRoot() {
        String storageRoot =getStorageRoot();
        String imageRoot = storageRoot + File.separator + "images";
        return imageRoot;
    }

    /**
     * 获取缓存根目录
     * @return
     */
    public static String getCacheRoot() {
        String storageRoot =getStorageRoot();
        String imageRoot = storageRoot + File.separator + "cache";
        return imageRoot;
    }




}
