package cn.shoptnt.framework.cache.standlone;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * EhCache缓存实现
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/10/19 17:09
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
@Primary
public class EhCacheImpl implements Cache {

    private static JdkSerializationRedisSerializer serializer = new JdkSerializationRedisSerializer();

    @Autowired
    private LockFactory lockFactory;

    @Override
    public Object get(Object key) {
        byte[] bytes = EhCacheFactory.CACHE_FACTORY.getCache().get(key.toString());
        Object value = deserialize(bytes);
        return value;
    }

    @Override
    public List multiGet(Collection keys) {
        ArrayList<Object> list = new ArrayList<>();
        for (Object key : keys) {
            Object value = this.get(key);
            list.add(value);
        }
        return list;
    }

    @Override
    public void multiSet(Map map) {
        org.ehcache.Cache ehCache = EhCacheFactory.CACHE_FACTORY.getCache();
        map.forEach((key, value) -> {
            byte[] bytes = this.serialize(value);
            ehCache.put(key.toString(), bytes);
        });
    }

    @Override
    public void multiDel(Collection keys) {
        org.ehcache.Cache ehCache = EhCacheFactory.CACHE_FACTORY.getCache();

        for (Object key : keys) {
            ehCache.remove(key.toString());
        }
    }

    @Override
    public void put(Object key, Object value) {
        byte[] bytes = this.serialize(value);
        EhCacheFactory.CACHE_FACTORY.getCache().put(key.toString(), bytes);
    }

    @Override
    public void put(Object key, Object value, int exp) {
        put(key, value);
    }


    @Override
    public void remove(Object key) {
        EhCacheFactory.CACHE_FACTORY.getCache().remove(key.toString());

    }

    @Override
    public void vagueDel(Object key) {
        EhCacheFactory.CACHE_FACTORY.getCache().remove(key.toString());
    }


    @Override
    public void putHash(Object key, Object hashKey, Object hashValue) {
        org.ehcache.Cache<String, byte[]> ehCache = EhCacheFactory.CACHE_FACTORY.getCache();
        byte[] bytes = ehCache.get(key.toString());
        HashMap hashMap = (HashMap) deserialize(bytes);
        if (hashMap == null) {
            hashMap = new HashMap();
        }
        hashMap.put(hashKey, hashValue);
        bytes = serialize(hashMap);
        ehCache.put(key.toString(), bytes);
    }

    @Override
    public void putAllHash(Object key, Map map) {
        org.ehcache.Cache<String, byte[]> ehCache = EhCacheFactory.CACHE_FACTORY.getCache();
        byte[] bytes = serialize(map);
        ehCache.put(key.toString(), bytes);
    }

    @Override
    public Object getHash(Object key, Object hashKey) {
        org.ehcache.Cache<String, byte[]> ehCache = EhCacheFactory.CACHE_FACTORY.getCache();
        byte[] bytes = ehCache.get(key.toString());
        HashMap hashMap = (HashMap) deserialize(bytes);
        if (MapUtils.isEmpty(hashMap)) {
            return null;
        }
        return hashMap.get(hashKey);
    }

    @Override
    public Map<Object, Object> getHash(Object key) {
        org.ehcache.Cache<String, byte[]> ehCache = EhCacheFactory.CACHE_FACTORY.getCache();
        byte[] bytes = ehCache.get(key.toString());
        HashMap hashMap = (HashMap) deserialize(bytes);
        return hashMap;
    }

    /**
     * 对某个key的值自增
     *
     * @param key
     * @param initialValue
     * @return
     */
    @Override
    public Long increment(Object key, Long initialValue) {

        Lock lock = lockFactory.getLock("lock_" + key);
        lock.lock();
        Long value = (Long) this.get(key);
        if (value == null) {
            value = initialValue;

        } else {
            value++;
        }
        this.put(key, value);
        lock.unlock();
        return value;
    }

    /**
     * 对象反序列化
     *
     * @param bytes
     * @return
     */
    private Object deserialize(byte[] bytes) {
        Object value = serializer.deserialize(bytes);

        return value;
    }

    /**
     * 对象序列化
     *
     * @param obj
     * @return
     */
    private byte[] serialize(Object obj) {
        byte[] value = serializer.serialize(obj);
        return value;
    }
}
