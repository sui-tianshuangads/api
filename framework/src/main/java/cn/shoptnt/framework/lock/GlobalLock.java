package cn.shoptnt.framework.lock;

import java.util.concurrent.locks.Lock;

/**
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年10月14日 17:00
 */
public interface GlobalLock {

  /**
   * 获取锁
   * @param name
   * @return
   */
  Lock getLock(String name);


}
