/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.security;

import cn.shoptnt.framework.security.message.UserDisableMsg;

import javax.servlet.http.HttpServletRequest;

/**
 * 认证业务类
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019/12/27
 */
public interface AuthenticationService {


    /**
     * 认证
     * @param req
     */
    void auth(HttpServletRequest req);


    /**
     * 用户被禁用事件
     * @param userDisableMsg
     */
    void userDisableEvent(UserDisableMsg userDisableMsg);

}
