/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.sncreator;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 * Snowflake 实现的发号器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-11-22
 */

@Configuration
public class SnowflakeSnCreator implements SnCreator {

    @Autowired
    private IdentifierGenerator keyGenerator;

    @Override
    public Long create(int subCode) {

        return  keyGenerator.nextId(new Object()).longValue();
    }


}
