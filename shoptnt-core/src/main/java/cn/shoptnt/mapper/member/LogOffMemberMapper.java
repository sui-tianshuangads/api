/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.member;

import cn.shoptnt.model.member.dos.LogOffMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 注销会员Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-27
 */
public interface LogOffMemberMapper extends BaseMapper<LogOffMember> {


}
