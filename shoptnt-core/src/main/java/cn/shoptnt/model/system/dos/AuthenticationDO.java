/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.validation.annotation.Mobile;
import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 二次身份验证实体
 *
 * @author shenyanwu
 * @version v7.0
 * @since v7.0.0
 * 2021-11-19 20:38:26
 */
@TableName("es_authentication")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AuthenticationDO implements Serializable {

    private static final long serialVersionUID = -5598543030979450044L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 管理员id
     */
    @Schema(name = "admin_id", description = "管理员id")
    private Long adminId;
    /**
     * 是否开启密码验证
     */
    @Schema(name = "password_auth", description = "是否开启密码验证,1为开启,0为关闭")
    @Min(message = "必须为数字且,1为开启,0为关闭", value = 0)
    @Max(message = "必须为数字且,1为开启,0为关闭", value = 1)
    private Integer passwordAuth;

    /**
     * 密码
     */
    @Schema(name = "password", description = "密码")
    @JsonIgnore
    private String password;

    /**
     * 是否开启短信验证
     */
    @Schema(name = "message_auth", description = "是否开启短信验证,1为开启,0为关闭")
    @Min(message = "必须为数字且,1为开启,0为关闭", value = 0)
    @Max(message = "必须为数字且,1为开启,0为关闭", value = 1)
    private Integer messageAuth;

    /**
     * 手机号码
     */
    @Schema(name = "mobile", description = "手机号码")
    private String mobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Integer getPasswordAuth() {
        return passwordAuth;
    }

    public void setPasswordAuth(Integer passwordAuth) {
        this.passwordAuth = passwordAuth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getMessageAuth() {
        return messageAuth;
    }

    public void setMessageAuth(Integer messageAuth) {
        this.messageAuth = messageAuth;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "Auth{" +
                "id=" + id +
                ", adminId=" + adminId +
                ", passwordAuth=" + passwordAuth +
                ", password='" + password + '\'' +
                ", messageAuth=" + messageAuth +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
