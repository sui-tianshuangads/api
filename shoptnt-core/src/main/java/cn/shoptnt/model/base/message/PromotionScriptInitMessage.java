package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 促销活动脚本初始化消息
 *
 * @author zs
 * @since 2024-03-08
 **/
public class PromotionScriptInitMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -8761441921918746501L;

    @Override
    public String getExchange() {
        return AmqpExchange.PROMOTION_SCRIPT_INIT;
    }

}
