/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base;

/**
 * 字符常量
 *
 * @author fk
 * @version 2.0
 * @since 7.1.5
 * 2019-09-09 18：00
 */
public class CharacterConstant {


    /**
     * 字符*
     */
    public final static char WILDCARD_STAR = '*';


}
