package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 交易创建消息
 * @author zs
 * @since 2024-03-08
 **/
public class TradeCreateMessage implements DirectMessage, Serializable {
    private static final long serialVersionUID = 2323762323161940L;

    private  String cacheKey;

    public String getCacheKey() {
        return cacheKey;
    }

    public TradeCreateMessage(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.ORDER_CREATE;
    }
}
