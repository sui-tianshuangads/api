/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 取消订单VO
 * 当订单已付款未收货时，用户申请取消订单时使用
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-22
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RefundApplyVO implements Serializable {

    private static final long serialVersionUID = -7411639410731062677L;

    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号", required = true)
    private String orderSn;
    /**
     * 申请原因
     */
    @Schema(name = "reason", description = "申请原因", required = true)
    private String reason;
    /**
     * 接收取消结果短信通知的手机号码
     */
    @Schema(name = "mobile", description = "手机号码", required = true)
    private String mobile;
    /**
     * 退款金额
     */
    @Schema(name = "refund_price", description = "退款金额", required = true)
    private Double refundPrice;
    /**
     * 账号类型 ALIPAY：支付宝，WEIXINPAY：微信，BANKTRANSFER：银行转账
     */
    @Schema(name = "account_type", description = "账号类型 ALIPAY：支付宝，WEIXINPAY：微信，BANKTRANSFER：银行转账,BALANCE:退款至余额", allowableValues = "ALIPAY,WEIXINPAY,BANKTRANSFER,BALANCE")
    private String accountType;
    /**
     * 退款账号
     */
    @Schema(name = "return_account", description = "退款账号")
    private String returnAccount;
    /**
     * 银行名称
     */
    @Schema(name = "bank_name", description = "银行名称")
    private String bankName;
    /**
     * 银行账户
     */
    @Schema(name = "bank_account_number", description = "银行账户")
    private String bankAccountNumber;
    /**
     * 银行开户名
     */
    @Schema(name = "bank_account_name", description = "银行开户名")
    private String bankAccountName;
    /**
     * 银行开户行
     */
    @Schema(name = "bank_deposit_name", description = "银行开户行")
    private String bankDepositName;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getReturnAccount() {
        return returnAccount;
    }

    public void setReturnAccount(String returnAccount) {
        this.returnAccount = returnAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankDepositName() {
        return bankDepositName;
    }

    public void setBankDepositName(String bankDepositName) {
        this.bankDepositName = bankDepositName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RefundApplyVO that = (RefundApplyVO) o;
        return Objects.equals(orderSn, that.orderSn) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(refundPrice, that.refundPrice) &&
                Objects.equals(accountType, that.accountType) &&
                Objects.equals(returnAccount, that.returnAccount) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(bankAccountNumber, that.bankAccountNumber) &&
                Objects.equals(bankAccountName, that.bankAccountName) &&
                Objects.equals(bankDepositName, that.bankDepositName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderSn, reason, mobile, refundPrice, accountType, returnAccount, bankName, bankAccountNumber, bankAccountName, bankDepositName);
    }

    @Override
    public String toString() {
        return "RefundApplyVO{" +
                "orderSn='" + orderSn + '\'' +
                ", reason='" + reason + '\'' +
                ", mobile='" + mobile + '\'' +
                ", refundPrice=" + refundPrice +
                ", accountType='" + accountType + '\'' +
                ", returnAccount='" + returnAccount + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccountNumber='" + bankAccountNumber + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", bankDepositName='" + bankDepositName + '\'' +
                '}';
    }
}
