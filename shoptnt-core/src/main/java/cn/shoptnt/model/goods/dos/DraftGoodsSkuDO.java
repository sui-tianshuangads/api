/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 草稿商品sku实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-26 11:38:06
 */
@TableName("es_draft_goods_sku")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DraftGoodsSkuDO implements Serializable {

    private static final long serialVersionUID = 5684194304207265L;

    /**
     * 主键ID
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long draftSkuId;
    /**
     * 草稿id
     */
    @Schema(name = "draft_goods_id", description = "草稿id")
    private Long draftGoodsId;
    /**
     * 货号
     */
    @Schema(name = "sn", description = "货号")
    private String sn;
    /**
     * 总库存
     */
    @Schema(name = "quantity", description = "总库存")
    private Integer quantity;
    /**
     * 价格
     */
    @Schema(name = "price", description = "价格")
    private Double price;
    /**
     * 规格
     */
    @Schema(name = "specs", description = "规格")
    private String specs;
    /**
     * 成本
     */
    @Schema(name = "cost", description = "成本")
    private Double cost;
    /**
     * 重量
     */
    @Schema(name = "weight", description = "重量")
    private Double weight;

    public DraftGoodsSkuDO() {
    }

    public DraftGoodsSkuDO(GoodsSkuVO skuVO) {
        this.setDraftGoodsId(skuVO.getGoodsId());
        this.setSn(skuVO.getSn());
        this.setPrice(skuVO.getPrice());
        this.setCost(skuVO.getCost());
        this.setWeight(skuVO.getWeight());
        this.setQuantity(skuVO.getQuantity());
        this.setSpecs(skuVO.getSpecs());
    }

    @PrimaryKeyField
    public Long getDraftSkuId() {
        return draftSkuId;
    }

    public void setDraftSkuId(Long draftSkuId) {
        this.draftSkuId = draftSkuId;
    }

    public Long getDraftGoodsId() {
        return draftGoodsId;
    }

    public void setDraftGoodsId(Long draftGoodsId) {
        this.draftGoodsId = draftGoodsId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "DraftGoodsSkuDO [draftSkuId=" + draftSkuId + ", draftGoodsId=" + draftGoodsId + ", sn=" + sn
                + ", quantity=" + quantity + ", price=" + price + ", specs=" + specs + ", cost=" + cost + ", weight="
                + weight + "]";
    }


}
