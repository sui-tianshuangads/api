/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.dos.GoodsGalleryDO;
import cn.shoptnt.model.promotion.exchange.dos.ExchangeDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @author fk
 * @version v1.0
 * @Description: 商家查询商品使用
 * @date 2018/5/21 14:36
 * @since v7.0.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsVO implements Serializable {

    private static final long serialVersionUID = 3922135264669953741L;
    @Schema(description =  "id")
    private Long goodsId;

    @Schema(name = "category_id", description =  "分类id")
    private Long categoryId;

    @Schema(description =  "分类名称")
    private String categoryName;

    @Schema(name = "shop_cat_id", description =  "店铺分类id")
    private Long shopCatId;

    @Schema(name = "brand_id", description =  "品牌id")
    private Long brandId;

    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;

    @Schema(name = "sn", description =  "商品编号")
    private String sn;

    @Schema(name = "price", description =  "商品价格")
    private Double price;

    @Schema(name = "cost", description =  "成本价格")
    private Double cost;

    @Schema(name = "mktprice", description =  "市场价格")
    private Double mktprice;

    @Schema(name = "weight", description =  "重量")
    private Double weight;

    @Schema(name = "goods_transfee_charge", description =  "谁承担运费0：买家承担，1：卖家承担")
    private Integer goodsTransfeeCharge;

    @Schema(name = "intro", description =  "详情")
    private String intro;

    @Schema(name = "have_spec", description =  "是否有规格0没有1有")
    private Integer haveSpec;

    @Schema(name = "quantity", description =  "库存")
    private Integer quantity;

    @Schema(name = "market_enable", description =  "是否上架，1上架 0下架")
    private Integer marketEnable;

    @Schema(name = "goods_gallery_list", description =  "商品相册")
    private List<GoodsGalleryDO> goodsGalleryList;

    @Schema(name = "page_title", description =  "seo标题")
    private String pageTitle;

    @Schema(name = "meta_keywords", description =  "seo关键字")
    private String metaKeywords;

    @Schema(name = "meta_description", description =  "seo描述")
    private String metaDescription;

    @Schema(name = "template_id", description =  "运费模板id,不需要运费模板时值是0")
    private Long templateId;

    @Schema(description =  "商品是否审核，0 待审核，1 审核通过 2 未通过")
    private Integer isAuth;

    @Schema(description =  "可用库存")
    private Integer enableQuantity;

    @Schema(description =  "审核备注")
    private String authMessage;

    @Schema(name = "goods_type", description =  "商品类型NORMAL普通POINT积分")
    private String goodsType;

    @Schema(name = "exchange", description =  "积分兑换对象")
    private ExchangeDO exchange;
    @Schema(name = "category_ids", description =  "分类id数组")
    private Long[] categoryIds;

    @Schema(name = "promotion_tip", description =  "商品参与的促销活动提示")
    private String promotionTip;

    @Schema(name = "intro_list", description =  "商品移动端详情数据集合")
    private List<GoodsMobileIntroVO> introList;

    @Schema(name = "goods_video", description =  "商品视频")
    private String goodsVideo;

    public GoodsVO() {
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getMktprice() {
        return mktprice;
    }

    public void setMktprice(Double mktprice) {
        this.mktprice = mktprice;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getHaveSpec() {
        return haveSpec;
    }

    public void setHaveSpec(Integer haveSpec) {
        this.haveSpec = haveSpec;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<GoodsGalleryDO> getGoodsGalleryList() {
        return goodsGalleryList;
    }

    public void setGoodsGalleryList(List<GoodsGalleryDO> goodsGalleryList) {
        this.goodsGalleryList = goodsGalleryList;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }


    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;

    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAuthMessage() {
        return authMessage;
    }

    public void setAuthMessage(String authMessage) {
        this.authMessage = authMessage;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public ExchangeDO getExchange() {
        return exchange;
    }

    public void setExchange(ExchangeDO exchange) {
        this.exchange = exchange;
    }

    public Long getShopCatId() {
        return shopCatId;
    }

    public void setShopCatId(Long shopCatId) {
        this.shopCatId = shopCatId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Long[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Long[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getPromotionTip() {
        return promotionTip;
    }

    public void setPromotionTip(String promotionTip) {
        this.promotionTip = promotionTip;
    }

    public List<GoodsMobileIntroVO> getIntroList() {
        return introList;
    }

    public void setIntroList(List<GoodsMobileIntroVO> introList) {
        this.introList = introList;
    }

    public String getGoodsVideo() {
        return goodsVideo;
    }

    public void setGoodsVideo(String goodsVideo) {
        this.goodsVideo = goodsVideo;
    }

    @Override
    public String toString() {
        return "GoodsVO{" +
                "goodsId=" + goodsId +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", shopCatId=" + shopCatId +
                ", brandId=" + brandId +
                ", goodsName='" + goodsName + '\'' +
                ", sn='" + sn + '\'' +
                ", price=" + price +
                ", cost=" + cost +
                ", mktprice=" + mktprice +
                ", weight=" + weight +
                ", goodsTransfeeCharge=" + goodsTransfeeCharge +
                ", intro='" + intro + '\'' +
                ", haveSpec=" + haveSpec +
                ", quantity=" + quantity +
                ", marketEnable=" + marketEnable +
                ", goodsGalleryList=" + goodsGalleryList +
                ", pageTitle='" + pageTitle + '\'' +
                ", metaKeywords='" + metaKeywords + '\'' +
                ", metaDescription='" + metaDescription + '\'' +
                ", templateId=" + templateId +
                ", isAuth=" + isAuth +
                ", enableQuantity=" + enableQuantity +
                ", authMessage='" + authMessage + '\'' +
                ", goodsType='" + goodsType + '\'' +
                ", exchange=" + exchange +
                ", categoryIds=" + Arrays.toString(categoryIds) +
                ", promotionTip='" + promotionTip + '\'' +
                ", introList='" + introList +
                ", goodsVideo='" + goodsVideo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoodsVO goodsVO = (GoodsVO) o;

        return new EqualsBuilder()
                .append(goodsId, goodsVO.goodsId)
                .append(categoryId, goodsVO.categoryId)
                .append(categoryName, goodsVO.categoryName)
                .append(shopCatId, goodsVO.shopCatId)
                .append(brandId, goodsVO.brandId)
                .append(goodsName, goodsVO.goodsName)
                .append(sn, goodsVO.sn)
                .append(price, goodsVO.price)
                .append(cost, goodsVO.cost)
                .append(mktprice, goodsVO.mktprice)
                .append(weight, goodsVO.weight)
                .append(goodsTransfeeCharge, goodsVO.goodsTransfeeCharge)
                .append(intro, goodsVO.intro)
                .append(haveSpec, goodsVO.haveSpec)
                .append(quantity, goodsVO.quantity)
                .append(marketEnable, goodsVO.marketEnable)
                .append(goodsGalleryList, goodsVO.goodsGalleryList)
                .append(pageTitle, goodsVO.pageTitle)
                .append(metaKeywords, goodsVO.metaKeywords)
                .append(metaDescription, goodsVO.metaDescription)
                .append(templateId, goodsVO.templateId)
                .append(isAuth, goodsVO.isAuth)
                .append(enableQuantity, goodsVO.enableQuantity)
                .append(authMessage, goodsVO.authMessage)
                .append(goodsType, goodsVO.goodsType)
                .append(exchange, goodsVO.exchange)
                .append(categoryIds, goodsVO.categoryIds)
                .append(promotionTip, goodsVO.promotionTip)
                .append(introList, goodsVO.introList)
                .append(goodsVideo, goodsVO.goodsVideo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(goodsId)
                .append(categoryId)
                .append(categoryName)
                .append(shopCatId)
                .append(brandId)
                .append(goodsName)
                .append(sn)
                .append(price)
                .append(cost)
                .append(mktprice)
                .append(weight)
                .append(goodsTransfeeCharge)
                .append(intro)
                .append(haveSpec)
                .append(quantity)
                .append(marketEnable)
                .append(goodsGalleryList)
                .append(pageTitle)
                .append(metaKeywords)
                .append(metaDescription)
                .append(templateId)
                .append(isAuth)
                .append(enableQuantity)
                .append(authMessage)
                .append(goodsType)
                .append(exchange)
                .append(categoryIds)
                .append(promotionTip)
                .append(introList)
                .append(goodsVideo)
                .toHashCode();
    }

}


