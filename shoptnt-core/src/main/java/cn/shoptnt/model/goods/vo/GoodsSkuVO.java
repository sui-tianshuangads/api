/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.dos.DraftGoodsSkuDO;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.framework.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * 商品sku
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月21日 上午11:50:42
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsSkuVO extends GoodsSkuDO {

    /**
     *
     */
    private static final long serialVersionUID = -666090547834195127L;

    @Schema(name = "spec_list", description =  "规格列表")
    private List<SpecValueVO> specList;

    @Schema(name = "goods_transfee_charge", description = "谁承担运费0：买家承担，1：卖家承担", hidden = true)
    private Integer goodsTransfeeCharge;

    @Schema(description = "是否被删除 0 删除 1 未删除", hidden = true)
    private Integer disabled;

    @Schema(description = "上架状态  0下架 1上架", hidden = true)
    private Integer marketEnable;

    @Schema(name = "goods_type", description = "商品类型NORMAL普通POINT积分")
    private String goodsType;

    @Schema(description = "最后修改时间", hidden = true)
    private Long lastModify;

    @Schema(description = "运费脚本", hidden = true)
    private List<String> scripts;

    public List<SpecValueVO> getSpecList() {

        if (this.getSpecs() != null) {
            return JsonUtil.jsonToList(this.getSpecs(), SpecValueVO.class);
        }

        return specList;
    }

    public void setSpecList(List<SpecValueVO> specList) {
        this.specList = specList;
    }

    public GoodsSkuVO() {
    }

    public GoodsSkuVO(DraftGoodsSkuDO draftSku) {
        this.setCost(draftSku.getCost());
        this.setPrice(draftSku.getPrice());
        this.setQuantity(draftSku.getQuantity());
        this.setSkuId(draftSku.getDraftSkuId());
        this.setSn(draftSku.getSn());
        this.setWeight(draftSku.getWeight());
        this.setSpecList(JsonUtil.jsonToList(draftSku.getSpecs(), SpecValueVO.class));
    }


    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }

    public Long getLastModify() {
        return lastModify;
    }

    public void setLastModify(Long lastModify) {
        this.lastModify = lastModify;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    @JsonIgnore
    public List<String> getScripts() {
        return scripts;
    }

    public void setScripts(List<String> scripts) {
        this.scripts = scripts;
    }

    @Override
    public String toString() {
        return "GoodsSkuVO{" +
                "specList=" + specList +
                ", goodsTransfeeCharge=" + goodsTransfeeCharge +
                ", disabled=" + disabled +
                ", marketEnable=" + marketEnable +
                ", goodsType='" + goodsType + '\'' +
                ", lastModify=" + lastModify +
                ", scripts=" + scripts +
                '}';
    }
}
