package cn.shoptnt.model.goods.enums;

/**
 * 缓存中库存类型
 * 用于更新缓存等操作使用
 *
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年10月26日 14:34
 */
public enum StockType {


    /**
     * sku实际的库存，包含了待发货的
     */
    SKU_STOCK_ACTUAL,

    /**
     * sku可以售的库存，不包含待发货的
     */
    SKU_STOCK_ENABLE,

    /**
     * goods实际的库存，包含了待发货的
     */
    GOODS_STOCK_ACTUAL,

    /**
     * goods可以售的库存，不包含待发货的
     */
    GOODS_STOCK_ENABLE,

}
