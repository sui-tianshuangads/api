/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 会员问题咨询对象vo
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
public class MemberAskVO extends MemberAsk {

    @Schema(name = "goods_price", description =  "商品价格")
    private Double goodsPrice;

    @Schema(name = "praise_rate", description =  "商品好评率")
    private Double praiseRate;

    @Schema(name = "comment_num", description =  "商品评论数量")
    private Integer commentNum;

    @Schema(name = "first_reply", description =  "会员商品咨询首条回复")
    private AskReplyDO firstReply;

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Double getPraiseRate() {
        return praiseRate;
    }

    public void setPraiseRate(Double praiseRate) {
        this.praiseRate = praiseRate;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public AskReplyDO getFirstReply() {
        return firstReply;
    }

    public void setFirstReply(AskReplyDO firstReply) {
        this.firstReply = firstReply;
    }

    @Override
    public String toString() {
        return "MemberAskVO{" +
                "goodsPrice=" + goodsPrice +
                ", praiseRate=" + praiseRate +
                ", commentNum=" + commentNum +
                ", firstReply=" + firstReply +
                '}';
    }
}
