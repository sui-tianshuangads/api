/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;


import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 后台首页会员vo
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 14:27:48
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BackendMemberVO implements Serializable {


    /**
     * 昵称
     */
    @Schema(name = "nickname", description =  "昵称")
    private String nickname;
    /**
     * 邮箱
     */
    @Schema(name = "email", description =  "邮箱")
    private String email;
    /**
     * 会员注册时间
     */
    @Schema(name = "createTime", description =  "会员注册时间")
    private Long createTime;

    /**
     * 手机号码
     */
    @Schema(name = "mobile", description =  "手机号码", required = true)
    private String mobile;


    @Schema(name = "uname", description =  "会员用户名", required = true)
    private String uname;


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    @Override
    public String toString() {
        return "BackendMemberVO{" +
                "nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", createTime=" + createTime +
                ", mobile='" + mobile + '\'' +
                ", uname='" + uname + '\'' +
                '}';
    }
}