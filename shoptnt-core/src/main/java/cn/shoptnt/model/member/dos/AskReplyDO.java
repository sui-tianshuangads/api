/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 会员商品咨询回复实体
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@TableName(value = "es_ask_reply")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AskReplyDO implements Serializable {

    private static final long serialVersionUID = 5652460062027483880L;

    /**
     * 主键
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 会员咨询id
     */
    @Schema(name = "ask_id", description =  "会员咨询id")
    private Long askId;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 会员名称
     */
    @Schema(name = "member_name", description =  "会员名称")
    private String memberName;
    /**
     * 回复内容
     */
    @Schema(name = "content", description =  "回复内容")
    private String content;
    /**
     * 回复时间
     */
    @Schema(name = "reply_time", description =  "回复时间")
    private Long replyTime;
    /**
     * 是否匿名 YES:是，NO:否
     */
    @Schema(name = "anonymous", description =  "是否匿名 YES:是，NO:否")
    private String anonymous;
    /**
     * 审核状态 WAIT_AUDIT:待审核,PASS_AUDIT:审核通过,REFUSE_AUDIT:审核未通过
     */
    @Schema(name = "auth_status", description =  "审核状态 WAIT_AUDIT:待审核,PASS_AUDIT:审核通过,REFUSE_AUDIT:审核未通过")
    private String authStatus;
    /**
     * 删除状态 DELETED：已删除 NORMAL：正常
     */
    @Schema(name = "is_del", description =  "删除状态 DELETED：已删除 NORMAL：正常")
    private String isDel;
    /**
     * 是否已回复 YES：是，NO：否
     */
    @Schema(name = "reply_status", description =  "是否已回复 YES：是，NO：否")
    private String replyStatus;
    /**
     * 创建时间
     */
    @Schema(name = "create_time", description =  "创建时间")
    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAskId() {
        return askId;
    }

    public void setAskId(Long askId) {
        this.askId = askId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Long replyTime) {
        this.replyTime = replyTime;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

    public String getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AskReplyDO that = (AskReplyDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(askId, that.askId) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(memberName, that.memberName) &&
                Objects.equals(content, that.content) &&
                Objects.equals(replyTime, that.replyTime) &&
                Objects.equals(anonymous, that.anonymous) &&
                Objects.equals(authStatus, that.authStatus) &&
                Objects.equals(isDel, that.isDel) &&
                Objects.equals(replyStatus, that.replyStatus) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, askId, memberId, memberName, content, replyTime, anonymous, authStatus, isDel, replyStatus, createTime);
    }

    @Override
    public String toString() {
        return "AskReplyDO{" +
                "id=" + id +
                ", askId=" + askId +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", content='" + content + '\'' +
                ", replyTime=" + replyTime +
                ", anonymous='" + anonymous + '\'' +
                ", authStatus='" + authStatus + '\'' +
                ", isDel='" + isDel + '\'' +
                ", replyStatus='" + replyStatus + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
