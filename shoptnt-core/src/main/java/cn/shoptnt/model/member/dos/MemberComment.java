/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;


/**
 * 评论实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:19:14
 */
@TableName(value = "es_member_comment")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberComment implements Serializable {

    private static final long serialVersionUID = 4761968067308018L;

    /**
     * 评论主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long commentId;
    /**
     * 商品id
     */
    @Schema(name = "goods_id", description =  "商品id")
    private Long goodsId;
    /**
     * skuid
     */
    @Schema(name = "sku_id", description =  "skuid")
    private Long skuId;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 卖家id
     */
    @Schema(name = "seller_id", description =  "卖家id")
    private Long sellerId;
    /**
     * 会员名称
     */
    @Schema(name = "member_name", description =  "会员名称")
    private String memberName;
    /**
     * 会员头像
     */
    @Schema(name = "member_face", description =  "会员头像")
    private String memberFace;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;
    /**
     * 商品默认图片
     */
    @Schema(name = "goods_img", description =  "商品默认图片")
    private String goodsImg;
    /**
     * 评论内容
     */
    @Schema(name = "content", description =  "评论内容")
    private String content;
    /**
     * 评论时间
     */
    @Schema(name = "create_time", description =  "评论时间")
    private Long createTime;
    /**
     * 是否有图片 1 有 0 没有
     */
    @Schema(name = "have_image", description =  "是否有图片 1 有 0 没有")
    private Integer haveImage;
    /**
     * 状态  1 正常 0 删除
     */
    @Schema(name = "status", description =  "状态  1 正常 0 删除 ")
    private Integer status;
    /**
     * 好中差评
     */
    @Schema(name = "grade", description =  "好中差评")
    private String grade;
    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description =  "订单编号")
    private String orderSn;
    /**
     * 是否回复 1 是 0 否
     */
    @Schema(name = "reply_status", description =  "是否回复 1 是 0 否")
    private Integer replyStatus;

    @Schema(name = "audit_status",description =  "初评审核:待审核(WAIT_AUDIT),审核通过(PASS_AUDIT),审核拒绝(REFUSE_AUDIT)",required = false)
    private String auditStatus;

    @Schema(name = "comments_type",description =  "评论类型：初评(INITIAL),追评(ADDITIONAL)",required = false)
    private String commentsType;

    @Schema(name = "parent_id",description =  "初评id，默认为0",required = false)
    private Long parentId;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberFace() {
        return memberFace;
    }

    public void setMemberFace(String memberFace) {
        this.memberFace = memberFace;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getHaveImage() {
        return haveImage;
    }

    public void setHaveImage(Integer haveImage) {
        this.haveImage = haveImage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Integer getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(Integer replyStatus) {
        this.replyStatus = replyStatus;
    }


    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getCommentsType() {
        return commentsType;
    }

    public void setCommentsType(String commentsType) {
        this.commentsType = commentsType;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberComment that = (MemberComment) o;
        return Objects.equals(commentId, that.commentId) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(skuId, that.skuId) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(sellerId, that.sellerId) &&
                Objects.equals(memberName, that.memberName) &&
                Objects.equals(memberFace, that.memberFace) &&
                Objects.equals(goodsName, that.goodsName) &&
                Objects.equals(goodsImg, that.goodsImg) &&
                Objects.equals(content, that.content) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(haveImage, that.haveImage) &&
                Objects.equals(status, that.status) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(orderSn, that.orderSn) &&
                Objects.equals(replyStatus, that.replyStatus) &&
                Objects.equals(auditStatus, that.auditStatus) &&
                Objects.equals(commentsType, that.commentsType) &&
                Objects.equals(parentId, that.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, goodsId, skuId, memberId, sellerId, memberName, memberFace, goodsName, goodsImg, content, createTime, haveImage, status, grade, orderSn, replyStatus, auditStatus, commentsType, parentId);
    }

    @Override
    public String toString() {
        return "MemberComment{" +
                "commentId=" + commentId +
                ", goodsId=" + goodsId +
                ", skuId=" + skuId +
                ", memberId=" + memberId +
                ", sellerId=" + sellerId +
                ", memberName='" + memberName + '\'' +
                ", memberFace='" + memberFace + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsImg='" + goodsImg + '\'' +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", haveImage=" + haveImage +
                ", status=" + status +
                ", grade='" + grade + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", replyStatus=" + replyStatus +
                ", auditStatus='" + auditStatus + '\'' +
                ", commentsType='" + commentsType + '\'' +
                ", parentId=" + parentId +
                '}';
    }
}
