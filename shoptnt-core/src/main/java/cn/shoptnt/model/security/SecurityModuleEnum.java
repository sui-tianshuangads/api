/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

/**
 * 数据安全模块枚举
 *
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/20 11:03
 **/
public enum SecurityModuleEnum {

    /**
     * 订单
     */
    order("订单","orderSignScanTask"),
    /**
     * 交易
     */
    trade("交易","tradeSignScanTask"),
    /**
     * 会员
     */
    member("会员","memberSignScanTask"),
    /**
     * 商品
     */
    goods("商品","goodsSignScanTask"),
    /**
     * sku
     */
    sku("sku","skuSignScanTask"),
    /**
     * 预存款
     */
    deposit("预存款","memberWalletSignScanTask");


    private String moduleName;
    private String scanTaskName;

    SecurityModuleEnum(String name, String taskName) {
        this.moduleName = name;
        this.scanTaskName = taskName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getScanTaskName() {
        return scanTaskName;
    }
}
