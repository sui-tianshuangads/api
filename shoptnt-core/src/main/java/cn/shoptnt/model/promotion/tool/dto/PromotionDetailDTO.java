/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.tool.dto;

import cn.shoptnt.framework.database.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 促销信息DTO
 * @author Snow create in 2018/4/16
 * @version v2.0
 * @since v7.0.0
 */
public class PromotionDetailDTO {

    /**活动开始时间*/
    @Column(name = "start_time")
    @Schema(name="start_time",description = "活动开始时间")
    private Long startTime;

    /**活动结束时间*/
    @Column(name = "end_time")
    @Schema(name="end_time",description = "活动结束时间")
    private Long endTime;

    /**活动id*/
    @Column(name = "activity_id")
    @Schema(name="activity_id",description = "活动id")
    private Long activityId;

    /**促销工具类型*/
    @Column(name = "promotion_type")
    @Schema(name="promotion_type",description = "促销工具类型")
    private String promotionType;

    /**活动标题*/
    @Column(name = "title")
    @Schema(name="title",description = "活动标题")
    private String title;


    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "PromotionDetailDTO{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", activityId=" + activityId +
                ", promotionType='" + promotionType + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
