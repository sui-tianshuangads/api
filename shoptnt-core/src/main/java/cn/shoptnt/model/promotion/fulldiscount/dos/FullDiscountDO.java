/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.fulldiscount.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.validation.annotation.RichTextSafeDomain;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.*;
import java.io.Serializable;


/**
 * 满优惠活动实体<br/>
 * 默认1为选中，0为未选
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-21 10:42:06
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@TableName(value = "es_full_discount")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class FullDiscountDO implements Serializable {

    private static final long serialVersionUID = 5679579627685903L;

    /**活动id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long fdId;

    /**优惠门槛金额*/
    @NotNull(message = "请填写优惠门槛")
    @DecimalMax(value = "99999999.00",message = "优惠券门槛金额超出限制")
    @Schema(name="full_money",description = "优惠门槛金额",required=true)
    private Double fullMoney;

    /**活动是否减现金*/
    @Min(value = 0,message = "减现金参数有误")
    @Max(value = 1,message = "减现金参数有误")
    @Schema(name="is_full_minus",description = "活动是否减现金",example = "0为否,1为是")
    private Integer isFullMinus;

    /**减现金*/
    @Schema(name="minus_value",description = "减现金")
    private Double minusValue;

    /**是否打折*/
    @Min(value = 0,message = "打折参数有误")
    @Max(value = 1,message = "打折参数有误")
    @Schema(name="is_discount",description = "是否打折",example = "0为否,1为是")
    private Integer isDiscount;

    /**打多少折*/
    @Schema(name="discount_value",description = "打多少折")
    private Double discountValue;

    /**是否赠送*/
    @Min(value = 0,message = "赠送积分有误")
    @Max(value = 1,message = "赠送积分有误")
    @Schema(name="is_send_point",description = "是否赠送积分",example = "0为否,1为是")
    private Integer isSendPoint;

    /**积分数据*/
    @Schema(name="point_value",description = "赠送多少积分")
    private Integer pointValue;

    /**是否免邮*/
    @Min(value = 0,message = "免邮费参数有误")
    @Max(value = 1,message = "免邮费参数有误")
    @Schema(name="is_free_ship",description = "是否邮费",example = "0为否,1为是")
    private Integer isFreeShip;

    /**是否有赠品*/
    @Min(value = 0,message = "送赠品参数有误")
    @Max(value = 1,message = "送赠品参数有误")
    @Schema(name="is_send_gift",description = "是否有赠品",example = "0为否,1为是")
    private Integer isSendGift;

    /**赠品id*/
    @Schema(name="gift_id",description = "赠品id")
    private Long giftId;


    /**是否赠优惠券*/
    @Min(value = 0,message = "送优惠券参数有误")
    @Max(value = 1,message = "送优惠券参数有误")
    @Schema(name="is_send_bonus",description = "是否增优惠券",example = "0为否,1为是")
    private Integer isSendBonus;

    /**优惠券id*/
    @Schema(name="bonus_id",description = "优惠券id")
    private Long bonusId;

    /**活动开始时间*/
    @NotNull(message = "请填写活动起始时间")
    @Schema(name="start_time",description = "活动起始时间",required=true)
    private Long startTime;

    /**活动结束时间*/
    @NotNull(message = "请填写活动截止时间")
    @Schema(name="end_time",description = "活动截止时间",required=true)
    private Long endTime;

    /**活动标题*/
    @NotEmpty(message = "请填写活动标题")
    @Schema(name="title",description = "活动标题",required=true)
    private String title;

    /**商品参与方式*/
    @NotNull(message = "请选择商品参与方式，全部商品：1，部分商品：2")
    @Min(value = 1,message = "商品参与方式值不正确")
    @Max(value = 2,message = "商品参与方式值不正确")
    @Schema(name="range_type",description = "商品参与方式",required=true)
    private Integer rangeType;

    /**是否停用*/
    @Schema(name="disabled",description = "是否停用")
    private Integer disabled;

    /**活动说明*/
    @RichTextSafeDomain
    @Schema(name="description",description = "活动说明")
    private String description;

    /**商家id*/
    @Schema(name="seller_id",description = "商家id")
    private Long sellerId;

    @PrimaryKeyField
    public Long getFdId() {
        return fdId;
    }
    public void setFdId(Long fdId) {
        this.fdId = fdId;
    }

    public Double getFullMoney() {
        return fullMoney;
    }
    public void setFullMoney(Double fullMoney) {
        this.fullMoney = fullMoney;
    }

    public Double getMinusValue() {
        return minusValue;
    }
    public void setMinusValue(Double minusValue) {
        this.minusValue = minusValue;
    }

    public Integer getIsFullMinus() {
        return isFullMinus;
    }
    public void setIsFullMinus(Integer isFullMinus) {
        this.isFullMinus = isFullMinus;
    }

    public Integer getIsFreeShip() {
        return isFreeShip;
    }
    public void setIsFreeShip(Integer isFreeShip) {
        this.isFreeShip = isFreeShip;
    }

    public Integer getIsSendGift() {
        return isSendGift;
    }
    public void setIsSendGift(Integer isSendGift) {
        this.isSendGift = isSendGift;
    }

    public Integer getIsSendBonus() {
        return isSendBonus;
    }
    public void setIsSendBonus(Integer isSendBonus) {
        this.isSendBonus = isSendBonus;
    }

    public Long getGiftId() {
        return giftId;
    }
    public void setGiftId(Long giftId) {
        this.giftId = giftId;
    }

    public Long getBonusId() {
        return bonusId;
    }
    public void setBonusId(Long bonusId) {
        this.bonusId = bonusId;
    }

    public Integer getIsDiscount() {
        return isDiscount;
    }
    public void setIsDiscount(Integer isDiscount) {
        this.isDiscount = isDiscount;
    }

    public Double getDiscountValue() {
        return discountValue;
    }
    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }

    public Long getStartTime() {
        return startTime;
    }
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRangeType() {
        return rangeType;
    }
    public void setRangeType(Integer rangeType) {
        this.rangeType = rangeType;
    }

    public Integer getDisabled() {
        return disabled;
    }
    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getIsSendPoint() {
        return isSendPoint;
    }

    public void setIsSendPoint(Integer isSendPoint) {
        this.isSendPoint = isSendPoint;
    }

    public Integer getPointValue() {
        return pointValue;
    }

    public void setPointValue(Integer pointValue) {
        this.pointValue = pointValue;
    }

    @Override
    public String toString() {
        return "FullDiscountDO{" +
                "fdId=" + fdId +
                ", fullMoney=" + fullMoney +
                ", isFullMinus=" + isFullMinus +
                ", minusValue=" + minusValue +
                ", isDiscount=" + isDiscount +
                ", discountValue=" + discountValue +
                ", isSendPoint=" + isSendPoint +
                ", pointValue=" + pointValue +
                ", isFreeShip=" + isFreeShip +
                ", isSendGift=" + isSendGift +
                ", giftId=" + giftId +
                ", isSendBonus=" + isSendBonus +
                ", bonusId=" + bonusId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", title='" + title + '\'' +
                ", rangeType=" + rangeType +
                ", disabled=" + disabled +
                ", description='" + description + '\'' +
                ", sellerId=" + sellerId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        FullDiscountDO that = (FullDiscountDO) o;

        return new EqualsBuilder()
                .append(fdId, that.fdId)
                .append(fullMoney, that.fullMoney)
                .append(isFullMinus, that.isFullMinus)
                .append(minusValue, that.minusValue)
                .append(isDiscount, that.isDiscount)
                .append(discountValue, that.discountValue)
                .append(isFreeShip, that.isFreeShip)
                .append(isSendGift, that.isSendGift)
                .append(giftId, that.giftId)
                .append(isSendBonus, that.isSendBonus)
                .append(bonusId, that.bonusId)
                .append(startTime, that.startTime)
                .append(endTime, that.endTime)
                .append(title, that.title)
                .append(rangeType, that.rangeType)
                .append(disabled, that.disabled)
                .append(description, that.description)
                .append(sellerId, that.sellerId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fdId)
                .append(fullMoney)
                .append(isFullMinus)
                .append(minusValue)
                .append(isDiscount)
                .append(discountValue)
                .append(isFreeShip)
                .append(isSendGift)
                .append(giftId)
                .append(isSendBonus)
                .append(bonusId)
                .append(startTime)
                .append(endTime)
                .append(title)
                .append(rangeType)
                .append(disabled)
                .append(description)
                .append(sellerId)
                .toHashCode();
    }
}
