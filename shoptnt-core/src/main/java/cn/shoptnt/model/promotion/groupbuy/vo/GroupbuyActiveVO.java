/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.groupbuy.vo;

import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.groupbuy.enums.GroupBuyStatusEnum;
import cn.shoptnt.framework.util.DateUtil;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 团购活动VO
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@Schema
public class GroupbuyActiveVO extends GroupbuyActiveDO {

    @Schema(name="status_text",description = "状态值")
    private String statusText;

    @Schema(name = "status",description =  "活动状态标识,expired表示已失效")
    private String status;

    public String getStatusText() {
        Long now = DateUtil.getDateline();
        if(this.getStartTime() > now) {
            return GroupBuyStatusEnum.NOT_BEGIN.getStatus();
        }else if(this.getStartTime()<now&&this.getEndTime()>now) {
            return GroupBuyStatusEnum.CONDUCT.getStatus();
        }else {
            return GroupBuyStatusEnum.OVERDUE.getStatus();
        }
    }

    public String getStatus() {
        Long now = DateUtil.getDateline();
        if(this.getStartTime() > now) {
            return null;
        }else if(this.getStartTime()<now&&this.getEndTime()>now) {
            return null;
        }else {
            return "expired";
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    @Override
    public String toString() {
        return "GroupbuyActiveVO{" +
                "statusText='" + statusText + '\'' +
                '}';
    }
}
