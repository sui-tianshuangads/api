/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.sign.enums;

/**
 * 签到活动状态
 *
 * @author fk
 * @version v6.4
 * @since v6.4
 * 2017年12月15日 下午3:32:43
 */
public enum SignInActivityStatus {
    /**
     * 未开始
     */
    WAIT("未开始"),
    /**
     * 已关闭
     */
    CLOSE("已关闭"),

    /**
     * 已开启
     */
    OPEN("已开启"),

    /**
     * 已结束
     */
    FINISH("已结束");

    private String description;

    SignInActivityStatus(String description) {
        this.description = description;

    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
