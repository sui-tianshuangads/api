package cn.shoptnt.model.promotion.sign.vos;

import cn.shoptnt.model.promotion.sign.dos.SignInActivity;
import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInActiveInfo
 * @description 签到活动详细接口
 * @program: api
 * 2024/3/12 17:18
 */
public class SignInActivityInfo extends SignInActivity implements Serializable {
    private static final long serialVersionUID = -7166304154840556011L;

    @Schema(description =  "签到奖品")
    List<SignInReward> signInRewards;

    public List<SignInReward> getSignInRewards() {
        return signInRewards;
    }

    public void setSignInRewards(List<SignInReward> signInRewards) {
        this.signInRewards = signInRewards;
    }
}
