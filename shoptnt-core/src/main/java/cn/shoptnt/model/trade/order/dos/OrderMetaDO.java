/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * 订单扩展信息表实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-10 14:22:57
 */
@TableName("es_order_meta")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderMetaDO implements Serializable {

    private static final long serialVersionUID = 3108120423695585L;

    /**主键ID*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long metaId;

    /**订单编号*/
    @Column(name = "order_sn")
    @Schema(name="order_sn",description="订单编号")
    private String orderSn;

    /**扩展-键*/
    @Column(name = "meta_key")
    @Schema(name="meta_key",description="扩展-键")
    private String metaKey;

    /**扩展-值*/
    @Column(name = "meta_value")
    @Schema(name="meta_value",description="扩展-值")
    private String metaValue;

    /**售后状态*/
    @Column(name = "status")
    @Schema(name="status",description="售后状态")
    private String status;

    @PrimaryKeyField
    public Long getMetaId() {
        return metaId;
    }
    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }


    public String getMetaKey() {
        return metaKey;
    }
    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        OrderMetaDO that = (OrderMetaDO) o;

        return new EqualsBuilder()
                .append(metaId, that.metaId)
                .append(orderSn, that.orderSn)
                .append(metaKey, that.metaKey)
                .append(metaValue, that.metaValue)
                .append(status, that.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(metaId)
                .append(orderSn)
                .append(metaKey)
                .append(metaValue)
                .append(status)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "OrderMetaDO{" +
                "metaId=" + metaId +
                ", orderSn=" + orderSn +
                ", metaKey='" + metaKey + '\'' +
                ", metaValue=" + metaValue +
                ", status=" + status +
                '}';
    }
}
