/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.cart.vo;

import cn.shoptnt.model.goods.vo.SpecValueVO;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 转化后的货品vo
 * @author Snow create in 2018/3/20
 * @version v2.0
 * @since v7.0.0
 */
public class TradeConvertGoodsSkuVO implements Serializable {


    @Schema(description = "可用库存")
    private Integer enableQuantity;

    @Schema(description = "货号")
    private Long skuId;

    @Schema(description = "商品id")
    private Long goodsId;

    @Schema(description = "商品名称")
    private String goodsName;

    @Schema(description = "商品编号")
    private String sn;

    @Schema(description = "库存")
    private Integer quantity;

    @Schema(description = "价格")
    private Double price;

    @Schema(description = "规格信息json")
    private String specs;

    @Schema(description = "成本价格")
    private Double cost;

    @Schema(description = "重量")
    private Double weight;

    @Schema(description = "分类id")
    private Long categoryId;

    @Schema(description = "缩略图")
    private String thumbnail;

    @Schema(description = "卖家名称")
    private String sellerName;

    @Schema(description = "卖家id")
    private Long sellerId;

    @Schema(description = "商品价格")
    private String goodsPrice;

    @Schema(description = "规格列表")
    private List<SpecValueVO> specList;

    @Schema(description = "谁承担运费0：买家承担，1：卖家承担")
    private Integer goodsTransfeeCharge;

    @Schema(description = "上架状态 1上架  0下架")
    private Integer marketEnable;

    @Schema(description = "是否被删除0 删除 1未删除")
    private Integer disabled;


    @Override
    public String toString() {
        return "TradeConvertGoodsSkuVO{" +
                "enableQuantity=" + enableQuantity +
                ", skuId=" + skuId +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", sn='" + sn + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", specs='" + specs + '\'' +
                ", cost=" + cost +
                ", weight=" + weight +
                ", categoryId=" + categoryId +
                ", thumbnail='" + thumbnail + '\'' +
                ", sellerName='" + sellerName + '\'' +
                ", sellerId=" + sellerId +
                ", goodsPrice='" + goodsPrice + '\'' +
                ", goodsTransfeeCharge=" + goodsTransfeeCharge +
                ", marketEnable=" + marketEnable +
                ", disabled=" + disabled +
                '}';
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;
    }

    public List<SpecValueVO> getSpecList() {
        return specList;
    }

    public void setSpecList(List<SpecValueVO> specList) {
        this.specList = specList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        TradeConvertGoodsSkuVO that = (TradeConvertGoodsSkuVO) o;

        return new EqualsBuilder()
                .append(enableQuantity, that.enableQuantity)
                .append(skuId, that.skuId)
                .append(goodsId, that.goodsId)
                .append(goodsName, that.goodsName)
                .append(sn, that.sn)
                .append(quantity, that.quantity)
                .append(price, that.price)
                .append(specs, that.specs)
                .append(cost, that.cost)
                .append(weight, that.weight)
                .append(categoryId, that.categoryId)
                .append(thumbnail, that.thumbnail)
                .append(sellerName, that.sellerName)
                .append(sellerId, that.sellerId)
                .append(goodsPrice, that.goodsPrice)
                .append(specList, that.specList)
                .append(goodsTransfeeCharge, that.goodsTransfeeCharge)
                .append(marketEnable, that.marketEnable)
                .append(disabled, that.disabled)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(enableQuantity)
                .append(skuId)
                .append(goodsId)
                .append(goodsName)
                .append(sn)
                .append(quantity)
                .append(price)
                .append(specs)
                .append(cost)
                .append(weight)
                .append(categoryId)
                .append(thumbnail)
                .append(sellerName)
                .append(sellerId)
                .append(goodsPrice)
                .append(specList)
                .append(goodsTransfeeCharge)
                .append(marketEnable)
                .append(disabled)
                .toHashCode();
    }
}
