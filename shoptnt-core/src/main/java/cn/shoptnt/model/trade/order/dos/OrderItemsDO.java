/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.goods.vo.SpecValueVO;
import cn.shoptnt.model.trade.cart.vo.CartSkuVO;
import cn.shoptnt.model.trade.order.enums.CommentStatusEnum;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.util.JsonUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;


/**
 * 订单货物表实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-10 16:05:21
 */
@TableName("es_order_items")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderItemsDO implements Serializable {

    private static final long serialVersionUID = 5055400562756190L;

    /**主键ID*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long itemId;
    /**商品ID*/
    @Column(name = "goods_id")
    @Schema(name="goods_id",description="商品ID")
    private Long goodsId;
    /**货品ID*/
    @Column(name = "product_id")
    @Schema(name="product_id",description="货品ID")
    private Long productId;
    /**销售量*/
    @Column(name = "num")
    @Schema(name="num",description="销售量")
    private Integer num;
    /**发货量*/
    @Column(name = "ship_num")
    @Schema(name="ship_num",description="发货量")
    private Integer shipNum;
    /**交易编号*/
    @Column(name = "trade_sn")
    @Schema(name="trade_sn",description="交易编号")
    private String tradeSn;
    /**订单编号*/
    @Column(name = "order_sn")
    @Schema(name="order_sn",description="订单编号")
    private String orderSn;
    /**图片*/
    @Column(name = "image")
    @Schema(name="image",description="图片")
    private String image;
    /**商品名称*/
    @Schema(name="name",description="商品名称")
    private String name;
    /**销售金额*/
    @Schema(name="price",description="销售金额")
    private Double price;
    /**分类ID*/
    @Schema(name="cat_id",description="分类ID")
    private Long catId;
    /**状态*/
    @Schema(name="state",description="状态")
    private Integer state;
    /**快照id*/
    @Schema(name="snapshot_id",description="快照id")
    private Long snapshotId;
    /**规格json*/
    @Schema(name="spec_json",description="规格json")
    private String specJson;
    /**促销类型*/
    @Column(name = "promotion_type")
    @Schema(name="promotion_type",description="促销类型")
    private String promotionType;
    /**促销id*/
    @Column(name = "promotion_id")
    @Schema(name="promotion_id",description="促销id")
    private Long promotionId;
    /**可退款金额*/
    @Column(name = "refund_price")
    @Schema(name="refund_price",description="可退款金额")
    private Double refundPrice;

    @Column(name = "comment_status")
    @Schema(name="comment_status",description="评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
    private String commentStatus;

    @PrimaryKeyField
    public Long getItemId() {
        return itemId;
    }
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getNum() {
        return num;
    }
    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getShipNum() {
        return shipNum;
    }
    public void setShipNum(Integer shipNum) {
        this.shipNum = shipNum;
    }

    public String getTradeSn() {
        return tradeSn;
    }
    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getOrderSn() {
        return orderSn;
    }
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCatId() {
        return catId;
    }
    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Integer getState() {
        return state;
    }
    public void setState(Integer state) {
        this.state = state;
    }

    public Long getSnapshotId() {
        return snapshotId;
    }
    public void setSnapshotId(Long snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getSpecJson() {
        return specJson;
    }
    public void setSpecJson(String specJson) {
        this.specJson = specJson;
    }

    public String getPromotionType() {
        return promotionType;
    }
    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public Long getPromotionId() {
        return promotionId;
    }
    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    @Override
    public String toString() {
        return "OrderItemsDO{" +
                "itemId=" + itemId +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", num=" + num +
                ", shipNum=" + shipNum +
                ", tradeSn='" + tradeSn + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", catId=" + catId +
                ", state=" + state +
                ", snapshotId=" + snapshotId +
                ", specJson='" + specJson + '\'' +
                ", promotionType='" + promotionType + '\'' +
                ", promotionId=" + promotionId +
                ", refundPrice=" + refundPrice +
                ", commentStatus='" + commentStatus + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderItemsDO that = (OrderItemsDO) o;

        return new EqualsBuilder()
                .append(itemId, that.itemId)
                .append(goodsId, that.goodsId)
                .append(productId, that.productId)
                .append(num, that.num)
                .append(shipNum, that.shipNum)
                .append(tradeSn, that.tradeSn)
                .append(orderSn, that.orderSn)
                .append(image, that.image)
                .append(name, that.name)
                .append(price, that.price)
                .append(catId, that.catId)
                .append(state, that.state)
                .append(snapshotId, that.snapshotId)
                .append(specJson, that.specJson)
                .append(promotionType, that.promotionType)
                .append(promotionId, that.promotionId)
                .append(refundPrice, that.refundPrice)
                .append(commentStatus, that.commentStatus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(itemId)
                .append(goodsId)
                .append(productId)
                .append(num)
                .append(shipNum)
                .append(tradeSn)
                .append(orderSn)
                .append(image)
                .append(name)
                .append(price)
                .append(catId)
                .append(state)
                .append(snapshotId)
                .append(specJson)
                .append(promotionType)
                .append(promotionId)
                .append(refundPrice)
                .append(commentStatus)
                .toHashCode();
    }

    public OrderItemsDO(){

    }

    /**
     * 订单货物构造器
     * @param skuVO
     */
    public OrderItemsDO(CartSkuVO skuVO){
        this.setGoodsId(skuVO.getGoodsId());
        this.setProductId(skuVO.getSkuId());
        this.setCatId(skuVO.getCatId());
        this.setImage(skuVO.getGoodsImage());
        this.setOrderSn(skuVO.getSkuSn());
        this.setName(skuVO.getName());
        this.setNum(skuVO.getNum());
        this.setPrice(skuVO.getPurchasePrice());
        List<SpecValueVO> specList = skuVO.getSpecList();
        String specJson = JsonUtil.objectToJson(specList);
        this.setSpecJson(specJson);
        this.setCommentStatus(CommentStatusEnum.UNFINISHED.name());

    }


    public OrderItemsDO(GoodsSkuVO skuVO) {
        this.setGoodsId(skuVO.getGoodsId());
        this.setProductId(skuVO.getSkuId());
        this.setCatId(skuVO.getCategoryId());
        this.setImage(skuVO.getThumbnail());
        this.setOrderSn(skuVO.getSn());
        this.setName(skuVO.getGoodsName());
        List<SpecValueVO> specList = skuVO.getSpecList();
        String specJson = JsonUtil.objectToJson(specList);
        this.setSpecJson(specJson);
        this.setCommentStatus(CommentStatusEnum.UNFINISHED.name());
    }
}
