/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.cart.dos;


/**
 * 订单权限
 * @author 妙贤
 * @version 1.0
 * @since v7.0.0
 * 2017年4月12日下午1:36:38
 */
public enum OrderPermission {

	/** 卖家权限*/
	seller,


	/** 买家权限*/
	buyer,

	/** 管理员权限*/
	admin,

	/** 客户端权限*/
	client


}
