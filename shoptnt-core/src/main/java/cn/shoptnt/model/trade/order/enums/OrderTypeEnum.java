/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.enums;

/**
 * Created by 妙贤 on 2019-01-27.
 * 订单类型
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-27
 */
public enum OrderTypeEnum {

    /**
     * 普通订单
     */
    NORMAL,

    /**
     * 拼团订单
     */
    PINTUAN,

    /**
     * 换货订单
     */
    CHANGE,

    /**
     * 补发商品订单
     */
    SUPPLY_AGAIN

}
