/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author fk
 * @version v2.0
 * @Description: 统计结果
 * @date 2018/4/2 79:16
 * @since v7.0.0
 */
public class BillResult implements Serializable {

    @Schema(name = "online_price", description = "在线支付的总金额")
    private Double onlinePrice;


    @Schema(name = "online_refund_price", description = "在线支付后退款的总金额")
    private Double onlineRefundPrice;

    @Schema(name = "cod_price", description = "货到付款的总金额")
    private Double codPrice;

    @Schema(name = "cod_refund_price", description = "货到付款后退款的总金额")
    private Double codRefundPrice;

    @Schema(name = "seller_id", description = "卖家id")
    private Long sellerId;

    @Schema(name = "site_coupon_commi", description = "站点优惠券佣金")
    private Double siteCouponCommi;


    public BillResult(Double onlinePrice, Double onlineRefundPrice, Double codPrice, Double codRefundPrice, Long sellerId,Double siteCouponCommi) {
        this.onlinePrice = onlinePrice;
        this.onlineRefundPrice = onlineRefundPrice;
        this.codPrice = codPrice;
        this.codRefundPrice = codRefundPrice;
        this.sellerId = sellerId;
        this.siteCouponCommi = siteCouponCommi;
    }

    public BillResult() {
    }

    public Double getOnlinePrice() {
        return onlinePrice;
    }

    public void setOnlinePrice(Double onlinePrice) {
        this.onlinePrice = onlinePrice;
    }

    public Double getOnlineRefundPrice() {
        return onlineRefundPrice;
    }

    public void setOnlineRefundPrice(Double onlineRefundPrice) {
        this.onlineRefundPrice = onlineRefundPrice;
    }

    public Double getCodPrice() {
        return codPrice;
    }

    public void setCodPrice(Double codPrice) {
        this.codPrice = codPrice;
    }

    public Double getCodRefundPrice() {
        return codRefundPrice;
    }

    public void setCodRefundPrice(Double codRefundPrice) {
        this.codRefundPrice = codRefundPrice;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Double getSiteCouponCommi() {
        return siteCouponCommi;
    }

    public void setSiteCouponCommi(Double siteCouponCommi) {
        this.siteCouponCommi = siteCouponCommi;
    }
}
