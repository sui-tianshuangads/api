/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.shop;

import cn.shoptnt.client.goods.GoodsIndexClient;
import cn.shoptnt.message.event.ShopChangeEvent;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.promotion.ExchangeGoodsClient;
import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.enums.GoodsType;
import cn.shoptnt.model.promotion.exchange.dos.ExchangeDO;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.shop.enums.ShopMessageTypeEnum;
import cn.shoptnt.model.shop.vo.ShopChangeMsg;
import cn.shoptnt.model.shop.vo.ShopVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 是否为自营变化处理
 *
 * @author zh
 * @version v7.0
 * @date 19/3/27 下午2:26
 * @since v7.0
 */
@Component
public class ShopSelfOperatedChanageConsumer implements ShopChangeEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private ExchangeGoodsClient exchangeGoodsClient;

    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Autowired
    private GoodsIndexClient goodsIndexClient;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void shopChange(ShopChangeMsg shopChangeMsg) {
        try {
            if (!StringUtil.equals(ShopMessageTypeEnum.All.value(), shopChangeMsg.getMessageType())) {
                return;
            }
            //原店铺数据
            ShopVO originalShop = shopChangeMsg.getOriginalShop();
            //更新后店铺数据
            ShopVO shop = shopChangeMsg.getNewShop();
            //如果原店铺是自营改变为普通商家则进行处理
            if (originalShop.getSelfOperated().equals(1) && shop.getSelfOperated().equals(0)) {
                //查询此店铺的所有积分商品
                List<GoodsDO> goods = goodsClient.listPointGoods(shop.getShopId());
                for (GoodsDO goodsDO : goods) {
                    ExchangeDO exchangeDO = exchangeGoodsClient.getModelByGoods(goodsDO.getGoodsId());
                    if (exchangeDO != null) {
                        //删除此店铺的所有积分商品
                        exchangeGoodsClient.del(goodsDO.getGoodsId());
                        //删除此店铺的所有积分活动
                        promotionGoodsClient.delPromotionGoods(goodsDO.getGoodsId(), PromotionTypeEnum.EXCHANGE.name(), exchangeDO.getExchangeId());
                    }
                }
                //更改所有的商品为普通商品
                goodsClient.updateGoodsType(shop.getShopId(), GoodsType.NORMAL.name());
            }
            //更新商品是否是自营 重新生成商品索引
            goodsClient.updateGoodsSelfOperated(shop.getShopId(), shop.getSelfOperated());
            //重新生成商品索引
            List<Map<String, Object>> goods = goodsClient.getGoodsAndParams(shop.getShopId());
            if (goods.size() > 0) {
                for (int i = 0; i < goods.size(); i++) {
                    goodsIndexClient.updateIndex(goods.get(i));
                }
            }
        } catch (Exception e) {
            logger.error("处理店铺类型改变出错" + e.getMessage());
        }
    }
}
