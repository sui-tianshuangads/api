package cn.shoptnt.message.standlone.member;

import cn.shoptnt.message.dispatcher.member.MemberInfoCompleteDispatcher;
import cn.shoptnt.model.base.message.MemberInfoCompleteMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 会员完善个人信息 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class MemberInfoCompleteListener {

    @Autowired
    private MemberInfoCompleteDispatcher memberInfoCompleteDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void memberInfoComplete(MemberInfoCompleteMessage message) {
        memberInfoCompleteDispatcher.dispatch(message);
    }
}
