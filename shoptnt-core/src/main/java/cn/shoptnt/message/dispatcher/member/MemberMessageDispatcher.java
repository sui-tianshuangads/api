package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberMessageEvent;
import cn.shoptnt.model.base.message.MemberSiteMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员站内消息 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberMessageDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberMessageEvent> events;

    public void dispatch(MemberSiteMessage message) {
        if (events != null) {
            for (MemberMessageEvent event : events) {
                try {
                    event.memberMessage(message.getMessageId());
                } catch (Exception e) {
                    logger.error("站内消息发送出错", e);
                }
            }
        }
    }
}
