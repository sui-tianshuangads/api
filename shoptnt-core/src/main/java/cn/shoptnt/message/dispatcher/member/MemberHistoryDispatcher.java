package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberHistoryEvent;
import cn.shoptnt.model.member.dto.HistoryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员足迹 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberHistoryDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberHistoryEvent> events;

    public void dispatch(HistoryDTO historyDTO) {
        if (events != null) {
            for (MemberHistoryEvent event : events) {
                try {
                    event.addMemberHistory(historyDTO);
                } catch (Exception e) {
                    logger.error("记录会员足迹消息出错", e);
                }
            }
        }
    }
}
