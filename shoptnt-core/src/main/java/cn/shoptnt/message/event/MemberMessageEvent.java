/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

/**
 * 站内消息
 *
 * @author fk
 * @version v2.0
 * @since v7.0 2018年3月23日 上午10:16:59
 */
public interface MemberMessageEvent {

    /**
     * 会员站内消息消费
     *
     * @param messageId
     */
    void memberMessage(Long messageId);
}
