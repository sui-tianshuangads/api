/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment;

import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.model.payment.enums.ClientType;
import cn.shoptnt.model.payment.vo.PayBill;
import cn.shoptnt.model.payment.vo.PaymentMethodVO;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;

import java.util.List;
import java.util.Map;

/**
 * 全局统一支付接口
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-10
 */
public interface PaymentManager {


    /**
     * 支付
     * @param payBill
     * @return
     */
    Map pay(PayBill payBill);

    /**
     * 支付异步回调
     * @param paymentPluginId
     * @param clientType
     * @return
     */
    String payCallback(String paymentPluginId, ClientType clientType);


    /**
     * 获取客户端支持的支付方式
     * @param clientType  客户端类型
     * @return
     */
    List<PaymentMethodVO> queryPayments(String clientType);


    /**
     * 同步回调方法
     *
     * @param tradeType
     * @param paymentPluginId
     */
    void payReturn(TradeTypeEnum tradeType, String paymentPluginId);


    /**
     * 查询支付结果并更新账单状态
     * @param subSn
     * @param serviceType
     * @return
     */
    String queryResult(String subSn,String serviceType);

    /**
     * 对一个子订单发起支付
     * @param param
     * @return
     */
    Map pay(PayParam param);
}
