/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.service.impl;


import cn.hutool.core.util.ObjectUtil;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.FileUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.dto.FileDTO;
import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.base.vo.FileVO;
import cn.shoptnt.model.errorcode.SystemErrorCode;
import cn.shoptnt.model.system.vo.UploaderVO;
import cn.shoptnt.service.base.plugin.upload.Uploader;
import cn.shoptnt.service.base.service.FileManager;
import cn.shoptnt.service.system.factory.UploadFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件上传接口实现
 *
 * @author zh
 * @version v2.0
 * @since v7.0
 * 2018年3月19日 下午4:38:42
 */
@Service
public class FileManagerImpl implements FileManager {

    @Autowired
    private UploadFactory uploadFactory;
    @Autowired
    private Cache cache;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public FileVO upload(FileDTO input, String scene) {
        if (StringUtil.isEmpty(scene)) {
            scene = "normal";
        }
        Uploader uploader = uploadFactory.getUploader();
        return uploader.upload(input, scene, this.getconfig());
    }

    @Override
    public void deleteFile(String filePath) {
        Uploader uploader = uploadFactory.getUploader();
        uploader.deleteFile(filePath, this.getconfig());
    }

    @Override
    public FileVO uploadFile(MultipartFile file, String scene) {

        //使用此方法，验证后缀名由调用者判断，这里主要目前主要是给中台使用 2020年12月17日16:22:08 by fk
        try {
            if (file != null && file.getOriginalFilename() != null) {
                //文件类型
                String contentType = file.getContentType();
                logger.debug("++++++++++++++++++文件类型为：++++++++++++" + contentType);
                FileDTO input = new FileDTO();
                input.setName(file.getOriginalFilename());
                input.setStream(file.getInputStream());
                input.setExt("jpeg");
                if (scene == null) {
                    scene = "other";
                }
                return this.upload(input, scene);
            } else {
                throw new ResourceNotFoundException("没有文件");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FileVO upload(MultipartFile file, String scene) {
        if (ObjectUtil.isNotEmpty(file) && ObjectUtil.isNotEmpty(file.getOriginalFilename())){
            String originalFilename = file.getOriginalFilename();
            if (originalFilename.endsWith("pdf")){
                throw new ServiceException("","不能上传PDF文件");
            }
        }
        //文件类型
        String contentType = file.getContentType();
        //获取文件名称
        String ext = contentType.substring(contentType.lastIndexOf("/") + 1, contentType.length());
        //如果是图片类型 则验证不允许上传超过2M
        if (FileUtil.isAllowUpImg(ext)) {
            if (!FileUtil.checkFileSize(file.getSize(), 50, "M")) {
                throw new ServiceException(SystemErrorCode.E901.code(), "图片超过上传限制大小，请上传50M以内图片");
            }
        }
        if (!FileUtil.isAllowUpFile(ext)) {
            throw new ServiceException(SystemErrorCode.E901.code(), "不允许上传的文件格式，请上传gif,jpg,png,jpeg,mp4,pdf,mov格式文件。");
        }
        try {
            if (file != null && file.getOriginalFilename() != null) {
                FileDTO input = new FileDTO();
                input.setName(file.getOriginalFilename());
                input.setStream(file.getInputStream());
                //mov格式的contentType是video/quicktime
                input.setExt("quicktime".equals(ext)?"mov":ext);
                return this.upload(input, scene);
            } else {
                throw new ResourceNotFoundException("没有文件");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 获取存储方案配置
     *
     * @return
     */
    private Map getconfig() {
        UploaderVO upload = (UploaderVO) cache.get(CachePrefix.UPLOADER.getPrefix());
        if (StringUtil.isEmpty(upload.getConfig())) {
            return new HashMap<>(16);
        }
        Gson gson = new Gson();
        List<ConfigItem> list = gson.fromJson(upload.getConfig(), new TypeToken<List<ConfigItem>>() {
        }.getType());
        Map<String, String> result = new HashMap<>(16);
        if (list != null) {
            for (ConfigItem item : list) {
                result.put(item.getName(), StringUtil.toString(item.getValue()));
            }
        }
        return result;
    }


}
