/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.service.impl;

import cn.shoptnt.framework.context.request.ThreadContextHolder;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.payment.enums.ClientType;
import cn.shoptnt.service.base.plugin.validator.ValidatorPlugin;
import cn.shoptnt.service.base.service.ValidatorManager;
import cn.shoptnt.model.system.vo.ValidatorPlatformVO;
import cn.shoptnt.service.system.factory.ValidatorFactory;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 验证相关接口实现
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
@Service
public class ValidatorManagerImpl implements ValidatorManager {

    @Autowired
    private ValidatorFactory validatorFactory;

    @Autowired
    private Cache cache;

    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void validate() {
        HttpServletRequest req = ThreadContextHolder.getHttpRequest();

        //读取请求的客户端，因为小程序和uniapp只支持图片验证
        //默认由request参数中读取client，如果读不到，尝试由header中读取
        String client = req.getParameter("mini_client");
        if (StringUtil.isEmpty(client)) {
            client = req.getHeader("clientType");
        }
        if (ClientType.MINI.getClient().equals(client) || ClientType.UNIAPP.name().equals(client) ) {
            ValidatorPlugin plugin = validatorFactory.getCaptchaValidatorPlugin();
            plugin.onValidate(new HashMap<String, Object>());
        } else {
            ValidatorPlugin platform = validatorFactory.getValidatorPlugin();
            platform.onValidate(this.getConfig());
        }
    }

    /**
     * 将json参数转换为map格式
     *
     * @return 验证平台参数
     */
    private Map getConfig() {
        ValidatorPlatformVO validatorPlatformVO = (ValidatorPlatformVO) this.cache.get(CachePrefix.VALIDATOR_PLATFORM.getPrefix());
        List<ConfigItem> list = validatorPlatformVO.getConfigItems();

        Map<String, String> result = new HashMap<>(16);

        if (list != null && list.size() != 0) {
            for (ConfigItem item : list) {
                result.put(item.getName(), StringUtil.toString(item.getValue()));
            }
        }
        return result;

    }

}
