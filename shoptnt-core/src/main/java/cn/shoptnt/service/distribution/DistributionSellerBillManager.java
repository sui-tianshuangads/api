/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.distribution;


import cn.shoptnt.model.distribution.dos.DistributionOrderDO;
import cn.shoptnt.model.distribution.dto.DistributionSellerBillDTO;

import java.util.List;

/**
 *
 * 商家返现计算
 * @author liushuai
 * @version v1.0
 * @since v7.0
 * 2018/9/4 下午11:41
 * @Description:
 *
 */
public interface DistributionSellerBillManager {


    /**
     * 新增记录
     * @param distributionOrderDO 分销订单对象
     */
    void add(DistributionOrderDO distributionOrderDO);
    /**
     * 新增退款记录
     * @param distributionOrderDO 分销订单对象
     */
    void addRefund(DistributionOrderDO distributionOrderDO);

    /**
     * 商家返现统计
     * @param startTime 开始日期
     * @param endTime   结束日期
     * @return
     */
    List<DistributionSellerBillDTO> countSeller(Integer startTime, Integer endTime);

}
