package cn.shoptnt.service.promotion.sign;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.model.promotion.sign.dto.SignInRecordSearch;
import cn.shoptnt.model.promotion.sign.vos.SignInRecordPageVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title signInRecordManager
 * @description 签到记录业务类
 * @program: api
 * 2024/3/12 12:05
 */
public interface SignInRecordManager extends IService<SignInRecord> {


    /**
     * 分页查询签到记录
     *
     * @param page               页码
     * @param pageSize           每页数量
     * @param signInRecordSearch 查询关键字
     * @return WebPage
     */
    WebPage list(long page, long pageSize, SignInRecordSearch signInRecordSearch);


    /**
     * 根据条件查询签到记录
     *
     * @param signInRecordSearch 签到记录查询条件
     * @return 签到记录
     */
    List<SignInRecord> list(SignInRecordSearch signInRecordSearch);

    /**
     * 查询今天是否签到
     *
     * @param memberId   会员id
     * @param activityId 活动id
     * @return
     */
    List<SignInRecord> getCurrent(Long memberId, Long activityId);

}
