/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.halfprice.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.promotion.HalfPriceMapper;
import cn.shoptnt.model.base.message.PromotionScriptMsg;
import cn.shoptnt.framework.message.TimeExecute;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.promotion.halfprice.dos.HalfPriceDO;
import cn.shoptnt.model.promotion.halfprice.vo.HalfPriceVO;
import cn.shoptnt.service.promotion.halfprice.HalfPriceManager;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;
import cn.shoptnt.model.promotion.tool.dto.PromotionDetailDTO;
import cn.shoptnt.model.promotion.tool.enums.PromotionStatusEnum;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.promotion.tool.dto.PromotionGoodsDTO;
import cn.shoptnt.model.promotion.tool.enums.ScriptOperationTypeEnum;
import cn.shoptnt.service.promotion.tool.PromotionGoodsManager;
import cn.shoptnt.service.promotion.tool.impl.AbstractPromotionRuleManagerImpl;
import cn.shoptnt.service.promotion.tool.support.PromotionCacheKeys;
import cn.shoptnt.model.util.PromotionValid;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.trigger.Interface.TimeTrigger;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 第二件半价业务类
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-23 19:53:42
 */
@SuppressWarnings("Duplicates")
@Service
public class HalfPriceManagerImpl extends AbstractPromotionRuleManagerImpl implements HalfPriceManager {

    @Autowired
    private HalfPriceMapper halfPriceMapper;

    @Autowired
    private Cache cache;

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private TimeTrigger timeTrigger;

    /**
     * 查询第二件半价促销活动分页数据
     * @param page 页码
     * @param pageSize 每页数量
     * @param keywords 查询关键字
     * @return WebPage
     */
    @Override
    public WebPage list(long page, long pageSize, String keywords) {
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //获取商家ID
        Long sellerId = seller.getSellerId();

        QueryWrapper<HalfPriceVO> wrapper = new QueryWrapper();

        wrapper.eq("seller_id", sellerId);
        if (!StringUtil.isEmpty(keywords)) {
            wrapper.like("title", keywords);
        }
        wrapper.orderByDesc("hp_id");

        IPage<HalfPriceVO> iPage = halfPriceMapper.selectPageVO(new Page(page, pageSize), wrapper);

        WebPage webPage = PageConvert.convert(iPage);


        List<HalfPriceVO> halfPriceVOList = webPage.getData();
        for (HalfPriceVO halfPriceVO : halfPriceVOList) {
            long nowTime = DateUtil.getDateline();
            //当前时间小于活动的开始时间 则为活动未开始
            if (nowTime < halfPriceVO.getStartTime().longValue()) {
                halfPriceVO.setStatusText("活动未开始");
                halfPriceVO.setStatus(PromotionStatusEnum.WAIT.toString());

                //大于活动的开始时间，小于活动的结束时间
            } else if (halfPriceVO.getStartTime().longValue() < nowTime && nowTime < halfPriceVO.getEndTime()) {
                halfPriceVO.setStatusText("正在进行中");
                halfPriceVO.setStatus(PromotionStatusEnum.UNDERWAY.toString());

            } else {
                halfPriceVO.setStatusText("活动已失效");
                halfPriceVO.setStatus(PromotionStatusEnum.END.toString());
            }
        }

        return webPage;
    }

    /**
     * 添加第二件半价促销活动信息
     * @param halfPriceVO 第二件半价促销活动信息
     * @return halfPriceVO 第二件半价促销活动信息
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, Exception.class, ServiceException.class, NoPermissionException.class})
    public HalfPriceVO add(HalfPriceVO halfPriceVO) {
        //检测开始时间和结束时间
        PromotionValid.paramValid(halfPriceVO.getStartTime(), halfPriceVO.getEndTime(), 1, null);
        this.verifyTime(halfPriceVO.getStartTime(), halfPriceVO.getEndTime(), PromotionTypeEnum.HALF_PRICE, null);

        //初步形成商品的DTO列表
        List<PromotionGoodsDTO> goodsDTOList = new ArrayList<>();
        //是否是全部商品参与
        if (halfPriceVO.getRangeType() == 1) {
            PromotionGoodsDTO goodsDTO = new PromotionGoodsDTO();
            goodsDTO.setGoodsId(-1L);
            goodsDTO.setSkuId(-1L);
            goodsDTO.setGoodsName("全部商品");
            goodsDTO.setThumbnail("");
            goodsDTOList.add(goodsDTO);
            halfPriceVO.setGoodsList(goodsDTOList);
        }

        this.verifyRule(halfPriceVO.getGoodsList());

        HalfPriceDO halfPriceDO = new HalfPriceDO();
        BeanUtils.copyProperties(halfPriceVO, halfPriceDO);
        this.halfPriceMapper.insert(halfPriceDO);

        Long id = halfPriceDO.getHpId();
        halfPriceVO.setHpId(id);
        PromotionDetailDTO detailDTO = new PromotionDetailDTO();
        detailDTO.setStartTime(halfPriceVO.getStartTime());
        detailDTO.setEndTime(halfPriceVO.getEndTime());
        detailDTO.setActivityId(halfPriceVO.getHpId());
        detailDTO.setPromotionType(PromotionTypeEnum.HALF_PRICE.name());
        detailDTO.setTitle(halfPriceVO.getTitle());

        //将活动商品入库
        this.promotionGoodsManager.add(halfPriceVO.getGoodsList(), detailDTO);

        cache.put(PromotionCacheKeys.getHalfPriceKey(halfPriceVO.getHpId()), halfPriceDO);

        //启用延时任务创建促销活动脚本信息
        PromotionScriptMsg promotionScriptMsg = new PromotionScriptMsg();
        promotionScriptMsg.setPromotionId(id);
        promotionScriptMsg.setPromotionName(halfPriceDO.getTitle());
        promotionScriptMsg.setPromotionType(PromotionTypeEnum.HALF_PRICE);
        promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.CREATE);
        promotionScriptMsg.setEndTime(halfPriceDO.getEndTime());
        String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.HALF_PRICE.name() + "}_" + id;
        timeTrigger.add(TimeExecute.SELLER_PROMOTION_SCRIPT_EXECUTER, promotionScriptMsg, halfPriceDO.getStartTime(), uniqueKey);

        return halfPriceVO;
    }

    /**
     * 修改第二件半价促销活动信息
     * @param halfPriceVO 第二件半价促销活动信息
     * @param id 第二件半价促销活动主键ID
     * @return halfPriceVO 第二件半价促销活动信息
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, Exception.class, ServiceException.class, NoPermissionException.class})
    public HalfPriceVO edit(HalfPriceVO halfPriceVO, Long id) {

        this.verifyStatus(id);

        this.verifyTime(halfPriceVO.getStartTime(), halfPriceVO.getEndTime(), PromotionTypeEnum.HALF_PRICE, id);
        //检测开始时间和结束时间
        PromotionValid.paramValid(halfPriceVO.getStartTime(), halfPriceVO.getEndTime(), 1, null);
        //获取修改操作之前的第二件半价促销活动信息
        HalfPriceDO oldHalfPrice = this.getFromDB(id);

        //初步形成商品的DTO列表
        List<PromotionGoodsDTO> goodsDTOList = new ArrayList<>();
        //是否是全部商品参与
        if (halfPriceVO.getRangeType() == 1) {
            PromotionGoodsDTO goodsDTO = new PromotionGoodsDTO();
            goodsDTO.setGoodsId(-1L);
            goodsDTO.setSkuId(-1L);
            goodsDTO.setGoodsName("全部商品");
            goodsDTO.setThumbnail("");
            goodsDTOList.add(goodsDTO);
            halfPriceVO.setGoodsList(goodsDTOList);
        }

        this.verifyRule(halfPriceVO.getGoodsList());

        Seller seller = UserContext.getSeller();
        Long sellerId = seller.getSellerId();

        halfPriceVO.setSellerId(sellerId);
        HalfPriceDO halfPriceDO = new HalfPriceDO();
        BeanUtils.copyProperties(halfPriceVO, halfPriceDO);
        halfPriceDO.setHpId(id);
        this.halfPriceMapper.updateById(halfPriceDO);
        PromotionDetailDTO detailDTO = new PromotionDetailDTO();
        detailDTO.setStartTime(halfPriceVO.getStartTime());
        detailDTO.setEndTime(halfPriceVO.getEndTime());
        detailDTO.setActivityId(halfPriceVO.getHpId());
        detailDTO.setPromotionType(PromotionTypeEnum.HALF_PRICE.name());
        detailDTO.setTitle(halfPriceVO.getTitle());

        //将活动商品入库
        this.promotionGoodsManager.edit(halfPriceVO.getGoodsList(), detailDTO);

        cache.put(PromotionCacheKeys.getHalfPriceKey(halfPriceVO.getHpId()), halfPriceDO);

        //启用延时任务创建促销活动脚本信息
        PromotionScriptMsg promotionScriptMsg = new PromotionScriptMsg();
        promotionScriptMsg.setPromotionId(id);
        promotionScriptMsg.setPromotionName(halfPriceDO.getTitle());
        promotionScriptMsg.setPromotionType(PromotionTypeEnum.HALF_PRICE);
        promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.CREATE);
        promotionScriptMsg.setEndTime(halfPriceDO.getEndTime());
        String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.HALF_PRICE.name() + "}_" + id;
        timeTrigger.edit(TimeExecute.SELLER_PROMOTION_SCRIPT_EXECUTER, promotionScriptMsg, oldHalfPrice.getStartTime(), halfPriceDO.getStartTime(), uniqueKey);

        return halfPriceVO;
    }

    /**
     * 删除第二件半价促销活动信息
     * @param id 第二件半价促销活动主键ID
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {RuntimeException.class, Exception.class, ServiceException.class, NoPermissionException.class})
    public void delete(Long id) {
        //验证此活动是否可进行编辑删除操作
        this.verifyStatus(id);

        //获取第二件半价促销活动信息
        HalfPriceDO halfPriceDO = this.getFromDB(id);
        halfPriceMapper.deleteById(id);
        //删除活动关系对照表
        this.promotionGoodsManager.delete(id, PromotionTypeEnum.HALF_PRICE.name());
        this.cache.remove(PromotionCacheKeys.getHalfPriceKey(id));

        //删除缓存中的延时任务执行器
        String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.HALF_PRICE.name() + "}_" + id;
        timeTrigger.delete(TimeExecute.SELLER_PROMOTION_SCRIPT_EXECUTER, halfPriceDO.getStartTime(), uniqueKey);
        if (halfPriceDO.getEndTime().longValue() < DateUtil.getDateline()) {
            timeTrigger.delete(TimeExecute.SELLER_PROMOTION_SCRIPT_EXECUTER, halfPriceDO.getEndTime(), uniqueKey);
        }
    }

    /**
     * 获取第二件半价促销活动信息
     * @param id 第二件半价促销活动主键ID
     * @return halfPriceVO  第二件半价促销活动信息
     */
    @Override
    public HalfPriceVO getFromDB(Long id) {
        //读取缓存中的活动信息
        HalfPriceDO halfPriceDO = (HalfPriceDO) this.cache.get(PromotionCacheKeys.getHalfPriceKey(id));
        //如果为空从数据库中读取
        if (halfPriceDO == null) {

            halfPriceDO = this.halfPriceMapper.selectById(id);
        }
        //如果从缓存和数据库读取都是空，则抛出异常
        if (halfPriceDO == null) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "活动不存在");
        }

        HalfPriceVO halfPriceVO = new HalfPriceVO();
        BeanUtils.copyProperties(halfPriceDO, halfPriceVO);

        //读取此活动参与的商品
        List<PromotionGoodsDO> goodsDOList = this.promotionGoodsManager.getPromotionGoods(id, PromotionTypeEnum.HALF_PRICE.name());
        Long[] skuIds = new Long[goodsDOList.size()];
        for (int i = 0; i < goodsDOList.size(); i++) {
            skuIds[i] = goodsDOList.get(i).getSkuId();
        }

        //读取商品的信息
        List<GoodsSelectLine> goodsSelectLineList = this.goodsClient.querySkus(skuIds);
        List<PromotionGoodsDTO> goodsList = new ArrayList<>();

        for (GoodsSelectLine goodsSelectLine : goodsSelectLineList) {
            PromotionGoodsDTO goodsDTO = new PromotionGoodsDTO();
            BeanUtil.copyProperties(goodsSelectLine, goodsDTO);
            goodsList.add(goodsDTO);
        }

        halfPriceVO.setGoodsList(goodsList);

        return halfPriceVO;
    }

    /**
     * 验证操作权限<br/>
     * 如有问题直接抛出权限异常
     * @param id 第二件半价促销活动主键ID
     */
    @Override
    public void verifyAuth(Long id) {
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //根据ID获取第二件半价促销活动信息
        HalfPriceVO halfPriceVO = this.getFromDB(id);
        //验证越权操作
        if (halfPriceVO == null || !seller.getSellerId().equals(halfPriceVO.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }
    }

    /**
     * 根据状态获取第二件半价活动信息
     * @return
     */
    @Override
    public List<HalfPriceDO> selectNoEndHalfPrice() {
        return new QueryChainWrapper<>(halfPriceMapper).gt("end_time", DateUtil.getDateline()).list();
    }

    /**
     * 验证此活动是否可进行编辑删除操作<br/>
     * 如有问题则抛出异常
     *
     * @param halfPriceId 活动id
     */
    private void verifyStatus(Long halfPriceId) {
        //根据ID获取第二件半价促销活动信息
        HalfPriceVO halfPriceVO = this.getFromDB(halfPriceId);
        //获取当前时间戳
        long nowTime = DateUtil.getDateline();
        //如果活动起始时间小于现在时间，活动已经开始了。
        if (halfPriceVO.getStartTime().longValue() < nowTime && halfPriceVO.getEndTime().longValue() > nowTime) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "活动已经开始，不能进行编辑删除操作");
        }
    }

}
