package cn.shoptnt.service.promotion.sign.impl;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.promotion.sigin.SignInRecordMapper;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.model.promotion.sign.dto.SignInRecordSearch;
import cn.shoptnt.service.promotion.sign.SignInRecordManager;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecordManager
 * @description 签到记录实现
 * @program: api
 * 2024/3/12 14:10
 */
@Service
public class SignInRecordManagerImpl extends ServiceImpl<SignInRecordMapper, SignInRecord> implements SignInRecordManager {

    @Override
    public WebPage list(long page, long pageSize, SignInRecordSearch signInRecordSearch) {

        IPage<SignInRecord> iPage = lambdaQuery()
                .eq(signInRecordSearch.getMemberId() != null, SignInRecord::getMemberId, signInRecordSearch.getMemberId())
                .eq(signInRecordSearch.getSignActivityId() != null, SignInRecord::getSignActivityId, signInRecordSearch.getSignActivityId())
                .page(new Page<>(page, pageSize));

        return PageConvert.convert(iPage);
    }

    @Override
    public List<SignInRecord> list(SignInRecordSearch signInRecordSearch) {
        return lambdaQuery()
                .eq(signInRecordSearch.getMemberId() != null, SignInRecord::getMemberId, signInRecordSearch.getMemberId())
                .eq(signInRecordSearch.getSignActivityId() != null, SignInRecord::getSignActivityId, signInRecordSearch.getSignActivityId())
                .list();

    }

    @Override
    public List<SignInRecord> getCurrent(Long memberId, Long activityId) {

        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        Long startOfDayTimestamp = today.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() / 1000;
        Long endOfDayTimestamp = today.atTime(LocalTime.MAX).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() / 1000;

        return lambdaQuery()
                .eq(memberId != null, SignInRecord::getMemberId, memberId)
                .eq(activityId != null, SignInRecord::getSignActivityId, activityId)
                .ge(SignInRecord::getSignInDate, startOfDayTimestamp)
                .le(SignInRecord::getSignInDate, endOfDayTimestamp)
                .list();
    }
}
