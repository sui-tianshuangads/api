package cn.shoptnt.service.member.impl;

import cn.shoptnt.mapper.member.LogOffMemberMapper;
import cn.shoptnt.model.member.dos.LogOffMember;
import cn.shoptnt.service.member.LogOffMemberManager;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author zh
 * @version 1.0
 * @title LogOffMemberManager
 * @description 注销账户会员
 * @program: api
 * 2024/3/6 16:56
 */
@Service
public class LogOffMemberManagerImpl extends ServiceImpl<LogOffMemberMapper, LogOffMember> implements LogOffMemberManager {
}
