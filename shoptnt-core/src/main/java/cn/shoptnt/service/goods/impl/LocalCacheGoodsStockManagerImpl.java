package cn.shoptnt.service.goods.impl;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.goods.GoodsSkuMapper;
import cn.shoptnt.model.errorcode.GoodsErrorCode;
import cn.shoptnt.model.goods.dto.StockParam;
import cn.shoptnt.model.goods.enums.QuantityType;
import cn.shoptnt.model.goods.enums.StockType;
import cn.shoptnt.model.goods.vo.GoodsQuantityVO;
import cn.shoptnt.service.goods.GoodsManager;
import cn.shoptnt.service.goods.GoodsQuantityManager;
import cn.shoptnt.service.goods.impl.util.StockCacheKeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 单机版缓存实现
 *
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年10月25日 15:12
 */
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class LocalCacheGoodsStockManagerImpl implements GoodsQuantityManager {

    @Autowired
    private Cache cache;

    @Autowired
    private GoodsManager goodsManager;

    @Autowired
    private GoodsSkuMapper goodsSkuMapper;

    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Boolean updateSkuQuantity(List<GoodsQuantityVO> goodsQuantityList) {

        //将业务传过来的库存参数转换为缓存的库存参数
        List<StockParam> params = this.convertStockParams(goodsQuantityList);
        //设置缓存key信息以及更新的库存数量
        params.forEach(this::setCacheKeyAndQuantity);
        //设置库存
        for (StockParam param : params) {
            cache.put(param.getCacheKey(), param.getUpdateQuantity().toString());
        }
        //更新数据库库存
        return params.stream().allMatch(this.goodsManager::updateQuantity);
    }

    @Override
    public void syncDataBase() {
        // 因为更新缓存的同时更新了数据库，这里不需要同步
    }

    @Override
    public Map<String, Integer> fillCacheFromDB(Long skuId) {
        Map<String, Integer> map = this.goodsSkuMapper.queryQuantity(skuId);
        Integer enableNum = map == null ? 0 : map.get("enable_quantity");
        Integer actualNum = map == null ? 0 : map.get("quantity");

        cache.put(StockCacheKeyUtil.skuActualKey(skuId), "" + actualNum);
        cache.put(StockCacheKeyUtil.skuEnableKey(skuId), "" + enableNum);
        return map;
    }

    @Override
    public List<String> multiGet(List<String> keys) {
        return cache.multiGet(keys);
    }

    @Override
    public void setQuantity(String key, String quantity) {
        cache.put(key, quantity);
    }

    @Override
    public String getQuantity(String key) {
        return (String) cache.get(key);
    }

    private Integer getQuantity(Long stockSn, StockType type) {
        //获取缓存key
        String cacheKey = getCacheKey(stockSn, type);
        //获取库存
        String quantity = (String) cache.get(cacheKey);

        //表示缓存被击穿了 从数据库中获取
        if (StringUtil.isEmpty(quantity)) {
            quantity = this.goodsManager.getQuantity(stockSn, type).toString();
            //更新缓存库存
            cache.put(cacheKey, quantity);
        }
        return StringUtil.toInt(quantity, 0);
    }

    /**
     * 设置缓存key信息以及更新的库存数量
     *
     * @param param 库存参数
     */
    private void setCacheKeyAndQuantity(StockParam param) {
        //获取缓存key
        String cacheKey = this.getCacheKey(param.getStockSn(), param.getStockType());
        param.setCacheKey(cacheKey);
        //获取缓存中的库存数 (如果获取不到需要从db中获取)
        Integer cacheQuantity = this.getQuantity(param.getStockSn(), param.getStockType());
        //param.getQuantity() 正数为扣减库存 负数为增加库存
        int quantity = cacheQuantity + param.getQuantity();
        if (quantity < 0) {
            throw new ServiceException(GoodsErrorCode.E300.code(), "发送超卖");
        }
        //设置所要更新的库存
        param.setUpdateQuantity(quantity);
    }

    /**
     * 将业务传过来的库存参数转换为缓存的库存参数
     *
     * @param goodsQuantityList 库存更新参数
     * @return 缓存更新参数
     */
    private List<StockParam> convertStockParams(List<GoodsQuantityVO> goodsQuantityList) {
        //创建sku参数集合
        List<StockParam> stockParams = new ArrayList<>();

        goodsQuantityList.forEach(goodsQuantity -> {
            //设置goods库存信息
            StockParam stockGoodsParam = new StockParam();
            stockGoodsParam.setQuantity(goodsQuantity.getQuantity());
            stockGoodsParam.setStockSn(goodsQuantity.getGoodsId());
            StockType stockType = goodsQuantity.getQuantityType().equals(QuantityType.actual) ? StockType.GOODS_STOCK_ACTUAL : StockType.GOODS_STOCK_ENABLE;
            stockGoodsParam.setStockType(stockType);
            stockParams.add(stockGoodsParam);
            //设置sku库存信息
            StockParam stockSkuParam = new StockParam();
            stockSkuParam.setQuantity(goodsQuantity.getQuantity());
            stockSkuParam.setStockSn(goodsQuantity.getSkuId());
            stockType = goodsQuantity.getQuantityType().equals(QuantityType.actual) ? StockType.SKU_STOCK_ACTUAL : StockType.SKU_STOCK_ENABLE;
            stockSkuParam.setStockType(stockType);
            stockParams.add(stockSkuParam);

        });

        return stockParams;
    }

    private String getCacheKey(Long stockSn, StockType type) {
        if(type == StockType.SKU_STOCK_ACTUAL){
            return StockCacheKeyUtil.skuActualKey(stockSn);
        }
        if(type == StockType.SKU_STOCK_ENABLE){
            return StockCacheKeyUtil.skuEnableKey(stockSn);
        }
        if(type == StockType.GOODS_STOCK_ACTUAL){
            return StockCacheKeyUtil.goodsActualKey(stockSn);
        }
        return StockCacheKeyUtil.goodsEnableKey(stockSn);
    }
}
