package cn.shoptnt.service.goodssearch.impl;

import cn.shoptnt.framework.util.PathUtil;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * lucene 工厂
 * 提供对lucene的writer 和reader的获取
 * @author kingapex
 * @version 1.0
 * @data 2022/11/1 16:24
 **/
public class LuceneFactory {

    protected final static Logger logger = LoggerFactory.getLogger(LuceneFactory.class);


    /**
     * 获取indexReader
     *
     * @return
     * @throws IOException
     */
    public static IndexReader getIndexReader() throws IOException {
        // 指定索引文件的存储位置，索引具体的表现形式就是一组有规则的文件
        Directory directory = getDirectory();
        return DirectoryReader.open(directory);
    }

    /**
     * 获取indexWriter
     *
     * @return
     * @throws IOException
     */
    public static IndexWriter getIndexWriter() {

        for (int i = 0; i <= 3; i++) {
            try {
                return retryGetIndexWriter();
            } catch (Exception e) {
                logger.error("getIndexWriter error ", e);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }

            }
        }
        throw new RuntimeException("获取 indexwriter 3次，均失败");
    }

    private static  IndexWriter retryGetIndexWriter() throws IOException {

        // 指定索引文件的存储位置，索引具体的表现形式就是一组有规则的文件
        Directory directory = getDirectory();
        // 配置分词器
        Analyzer analyzer = new SmartChineseAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        // 创建IndexWriter对象，用于创建索引
        return new IndexWriter(directory, config);

    }

    /**
     * 获取索引存储路径
     *
     * @return
     * @throws IOException
     */
    private static Directory getDirectory() throws IOException {
        String indexPath = PathUtil.getIndexRoot();
        return FSDirectory.open(new File(indexPath).toPath());
    }


}
