/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goodssearch;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 商品索引管理接口
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 14:11:46
 */
public interface GoodsIndexManager {

    /**
     * 将某个商品加入索引
     *
     * @param goods
     * @throws IOException
     */
    boolean addIndex(Map goods) throws IOException;

    /**
     * 更新某个商品的索引
     *
     * @param goods 商品数据
     */
    void updateIndex(Map goods) throws IOException;


    /**
     * 更新
     *
     * @param goods 商品数据
     */
    void deleteIndex(Map goods) throws IOException;

    /**
     * 初始化索引
     *
     * @param list  索引数据
     * @param index 数量
     * @return 是否是生成成功
     */
    boolean addAll(List<Map<String, Object>> list, int index) throws IOException;


}
