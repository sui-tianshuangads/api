/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import cn.shoptnt.service.base.plugin.waybill.WayBillPlugin;
import cn.shoptnt.model.system.vo.WayBillVO;

/**
 * WayBill vo转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/2
 */

public class WayBillVoConverter {

    /**
     * 通过插件转换vo
     * @param plugin
     * @return
     */
    public static WayBillVO toValidatorPlatformVO(WayBillPlugin plugin) {
        WayBillVO vo = new WayBillVO();
        vo.setId(0L);
        vo.setName(plugin.getPluginName());
        vo.setOpen(plugin.getOpen());
        vo.setBean(plugin.getPluginId());
        vo.setConfigItems( plugin.definitionConfigItem());
        return vo;
    }


}
