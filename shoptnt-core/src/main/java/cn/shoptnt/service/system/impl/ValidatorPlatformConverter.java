/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import cn.shoptnt.service.base.plugin.validator.ValidatorPlugin;
import cn.shoptnt.model.system.vo.ValidatorPlatformVO;

/**
 * 验证码vo转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/2
 */

public class ValidatorPlatformConverter {

    /**
     * 通过插件转换vo
     * @param plugin
     * @return
     */
    public static ValidatorPlatformVO toValidatorPlatformVO(ValidatorPlugin plugin) {
        ValidatorPlatformVO vo = new ValidatorPlatformVO();
        vo.setId(0L);
        vo.setName(plugin.getPluginName());
        vo.setOpen(plugin.getIsOpen());
        vo.setPluginId(plugin.getPluginId());
        vo.setConfigItems( plugin.definitionConfigItem());
        return vo;
    }


}
