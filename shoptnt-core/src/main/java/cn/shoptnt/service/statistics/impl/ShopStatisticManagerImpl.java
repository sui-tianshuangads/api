/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.statistics.GoodsStatisticsMapper;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.errorcode.StatisticsErrorCode;
import cn.shoptnt.model.statistics.exception.StatisticsException;
import cn.shoptnt.service.statistics.Constant.TableNameConstant;
import cn.shoptnt.service.statistics.ShopStatisticManager;
import cn.shoptnt.service.statistics.util.SyncopateUtil;
import cn.shoptnt.util.DataDisplayUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * 店铺相关统计
 *
 * @author 张崧
 * @since 2024-04-24
 */
@Service
public class ShopStatisticManagerImpl implements ShopStatisticManager {

    @Autowired
    private GoodsStatisticsMapper goodsStatisticsMapper;

    @Override
    public WebPage getHotSalesMoneyPage(SearchCriteria searchCriteria) {
        searchCriteria = new SearchCriteria(searchCriteria);

        long[] timestamp = DataDisplayUtil.getStartTimeAndEndTime(searchCriteria);
        //开始时间
        long startTime = timestamp[0];
        //结束时间
        long endTime = timestamp[1];

        int limit = searchCriteria.getLimit() == null ? 50 : searchCriteria.getLimit();

        //表名后面拼接年份，如es_sss_order_goods_data_2020
        String tableNameEsSssOrderGoodsDataYear = SyncopateUtil.handleSql(searchCriteria.getYear(), TableNameConstant.ES_SSS_ORDER_GOODS_DATA);

        //表名后面拼接年份，如es_sss_order_data_2020
        String tableNameEsSssOrderDataYear = SyncopateUtil.handleSql(searchCriteria.getYear(), TableNameConstant.ES_SSS_ORDER_DATA);

        IPage iPage;

        try {
            //查询某一年的表
            iPage = goodsStatisticsMapper.selectShopHotSalesMoneyPage(new Page<>(1, limit), tableNameEsSssOrderGoodsDataYear,
                    tableNameEsSssOrderDataYear, startTime, endTime);

            //如果未查到数据再差总表
            if (iPage == null || iPage.getRecords() == null || iPage.getRecords().size() == 0) {
                iPage = goodsStatisticsMapper.selectShopHotSalesMoneyPage(new Page<>(1, limit), TableNameConstant.ES_SSS_ORDER_GOODS_DATA,
                        TableNameConstant.ES_SSS_ORDER_DATA, startTime, endTime);
            }
        } catch (MyBatisSystemException e) {//使用Mybatisplus分页查询，如果表不存在会抛MyBatisSystemException异常，而不是BadSqlGrammarException
            //某个年份的统计表不存在，则返回空数据
            return new WebPage(1L, 0L, 50L, new ArrayList());
        } catch (Exception e) {
            e.printStackTrace();
            throw new StatisticsException(StatisticsErrorCode.E810.code(), StatisticsErrorCode.E810.des());
        }

        return PageConvert.convert(iPage);
    }
}
