/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.shoptnt.mapper.statistics.SyncopateTableMapper;
import cn.shoptnt.service.statistics.SyncopateTableManager;
import cn.shoptnt.service.statistics.util.SyncopateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * SyncopateTableManagerImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 上午9:59
 */
@Service
public class SyncopateTableManagerImpl implements SyncopateTableManager {

    @Autowired
    private SyncopateTableMapper syncopateTableMapper;

    /**
     * 每日填充数据
     */
    @Override
    public void everyDay() {
        SyncopateUtil.createCurrentTable(syncopateTableMapper);
    }

}