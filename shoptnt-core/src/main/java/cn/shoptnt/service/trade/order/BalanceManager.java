/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.vo.BalancePayVO;

/**
*
* @description: 预存款抵扣业务层
* @author: liuyulei
* @create: 2020/1/1 11:53
* @version:1.0
* @since:7.1.5
**/
public interface BalanceManager {


    /**
     * 预存款抵扣方法
     * @param sn   交易编号/订单编号
     * @param memberId 会员id
     * @param tradeType 交易类型
     * @param password  交易密码
     * @return 预存款支付参数
     */
    BalancePayVO balancePay(String sn,Long memberId, String tradeType, String password);

}
