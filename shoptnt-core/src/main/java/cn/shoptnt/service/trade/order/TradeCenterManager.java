/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.vo.TradeParam;
import cn.shoptnt.model.trade.order.vo.TradeVO;

/**
 * 交易管理
 * @author liuyulei
 * @version v2.0
 * @since v7.1.5
 */
public interface TradeCenterManager {

    /**
     * 创建交易对象
     * @param tradeParam 交易参数
     * @return 交易VO
     */
    TradeVO createTrade(TradeParam tradeParam);

}
