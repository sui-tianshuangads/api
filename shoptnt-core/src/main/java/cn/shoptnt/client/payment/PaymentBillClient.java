/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.payment;

import cn.shoptnt.model.payment.dos.PaymentBillDO;
import cn.shoptnt.model.payment.vo.PayBill;

import java.util.Map;

/**
 * 支付账单client
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/16
 */
public interface PaymentBillClient {
    /**
     * 创建一个支付账单
     * @param paymentBill
     * @return
     */
    PayBill add(PaymentBillDO paymentBill);

    /**
     * 删除无用的支付账单
     *
     * @param billId 支付账单id
     */
    void deletePaymentBill(Long billId);

    /**
     * 关闭第三方交易
     *
     * @param paymentBillDO
     */
    void closeTrade(PaymentBillDO paymentBillDO);

    /**
     * 检测并且发送取消账单消息
     *
     * @param map key 为交易单号
     *            value 为支付类型
     */
    void sendCloseTradeMessage(Map<String, Object> map);
}
