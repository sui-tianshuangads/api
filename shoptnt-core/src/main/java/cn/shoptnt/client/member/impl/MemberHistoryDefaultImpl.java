/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberHistoryClient;
import cn.shoptnt.model.member.dto.HistoryDTO;
import cn.shoptnt.service.member.HistoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 会员历史
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午2:57
 * @since v7.0
 */

@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class MemberHistoryDefaultImpl implements MemberHistoryClient {

    @Autowired
    private HistoryManager historyManager;


    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addMemberHistory(HistoryDTO historyDTO) {
        historyManager.addMemberHistory(historyDTO);
    }


}
