/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberAddressClient;
import cn.shoptnt.model.member.dos.MemberAddress;
import cn.shoptnt.service.member.MemberAddressManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 会员地址默认实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午3:54
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class MemberAddressDefaultImpl implements MemberAddressClient {

    @Autowired
    private MemberAddressManager memberAddressManager;

    @Override
    public MemberAddress getModel(Long id) {
        return memberAddressManager.getModelFromXml(id);
    }

    @Override
    public MemberAddress getDefaultAddress(Long memberId) {
        return memberAddressManager.getDefaultAddress(memberId);
    }
}
