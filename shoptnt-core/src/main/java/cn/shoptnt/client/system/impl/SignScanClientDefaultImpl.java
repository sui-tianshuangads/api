/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.SignScanClient;
import cn.shoptnt.service.security.SignScanManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author zs
 * @version v1.0
 * @Description: 篡改记录
 * @date 2021-12-21
 * @since v5.2.3
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class SignScanClientDefaultImpl implements SignScanClient {

    @Autowired
    private SignScanManager signScanManager;

    @Override
    public void scan() {
        signScanManager.scan();
    }
}
