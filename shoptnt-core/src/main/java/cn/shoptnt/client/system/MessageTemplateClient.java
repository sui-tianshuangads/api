/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;

import cn.shoptnt.model.system.enums.MessageCodeEnum;
import cn.shoptnt.model.system.dos.MessageTemplateDO;
import cn.shoptnt.model.system.enums.WechatMsgTemplateTypeEnum;

import java.util.List;

/**
 * @version v7.0
 * @Description: 消息模版client
 * @Author: zjp
 * @Date: 2018/7/27 09:42
 */
public interface MessageTemplateClient {
    /**
     * 获取消息模版
     * @param messageCodeEnum 消息模版编码
     * @return MessageTemplateDO  消息模版
     */
    MessageTemplateDO getModel(MessageCodeEnum messageCodeEnum);

    /**
     * 发送消息
     * @param memberId
     * @param keywords
     * @param messageType
     */
    void sendWechatMsg(Long memberId, WechatMsgTemplateTypeEnum messageType, List<Object> keywords);

}
