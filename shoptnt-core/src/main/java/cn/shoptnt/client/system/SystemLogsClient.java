/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;


import cn.shoptnt.model.system.dos.SystemLogs;

/**
 * 系统日志client
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
public interface SystemLogsClient {


    /**
     * 添加系统日志
     * @param systemLogs
     */
    void add(SystemLogs systemLogs);

}
