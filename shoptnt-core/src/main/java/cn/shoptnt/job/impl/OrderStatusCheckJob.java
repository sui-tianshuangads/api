/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.client.trade.OrderTaskClient;
import cn.shoptnt.job.EveryHourExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单状态扫描
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-05 下午2:11
 */
@Component
public class OrderStatusCheckJob implements EveryHourExecute {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OrderTaskClient orderTaskClient;


    @Override
    public void everyHour() {
        /** 自动确认收货 */
        try {
            this.orderTaskClient.rogTask();
        } catch (Exception e) {
            logger.error("订单自动确认收货出错", e);
        }

        /** 自动完成天数 */
        try {
            this.orderTaskClient.completeTask();
        } catch (Exception e) {
            logger.error("订单自动标记为完成出错", e);
        }

        /** 自动支付天数 */
        try {
            this.orderTaskClient.payTask();
        } catch (Exception e) {
            logger.error("订单自动支付出错", e);
        }

        /** 售后失效天数 */
        try {
            this.orderTaskClient.serviceTask();
        } catch (Exception e) {
            logger.error("订单自动标记为售后过期出错", e);
        }

        /** 自动评价天数 */
        try {
            this.orderTaskClient.commentTask();
        } catch (Exception e) {
            logger.error("订单自动评价出错", e);
        }

        /** 自动交易投诉失效天数 */
        try {
            this.orderTaskClient.complainTask();
        } catch (Exception e) {
            logger.error("订单交易投诉失效出错", e);
        }
    }
}
