/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.dispatcher;

import cn.hutool.core.collection.CollUtil;
import cn.shoptnt.job.EveryTenMinutesExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 每十分钟执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Service
public class EveryTenMinutesDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<EveryTenMinutesExecute> everyTenMinutesExecute;

    public Boolean dispatch() {
        boolean result = true;
        if (CollUtil.isNotEmpty(everyTenMinutesExecute)) {
            try {
                everyTenMinutesExecute.forEach(EveryTenMinutesExecute::everyTenMinutes);
            } catch (Exception e) {
                logger.error("每十分钟任务异常：", e);
                result = false;
            }

        }
        return result;
    }
}
