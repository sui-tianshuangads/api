/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.dispatcher;

import cn.shoptnt.job.EveryMonthExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 每月执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Service
public class EveryMonthDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<EveryMonthExecute> everyMonthExecutes;

    public Boolean dispatch() {
        boolean result = true;
        if (everyMonthExecutes != null) {
            for (EveryMonthExecute everyMonthExecute : everyMonthExecutes) {
                try {
                    everyMonthExecute.everyMonth();
                } catch (Exception e) {
                    logger.error("每月任务异常：", e);
                    result = false;
                }
            }
        }
        return result;
    }
}
