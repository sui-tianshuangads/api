/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.content;

import cn.shoptnt.model.base.DomainSettings;
import cn.shoptnt.framework.test.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * Created by 妙贤 on 2018/6/20.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/6/20
 */
public class DomainSettingsTest extends BaseTest {

    @Autowired
    private DomainSettings domainSettings;

    @Test
    public void test() {
        Assert.notNull(domainSettings, "domainSettings 对象未能正确初始化");
    }

}
