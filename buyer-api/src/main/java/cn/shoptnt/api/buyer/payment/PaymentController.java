/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.payment;

import cn.shoptnt.model.base.DomainHelper;
import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.model.payment.enums.ClientType;
import cn.shoptnt.model.payment.enums.PayMode;
import cn.shoptnt.model.payment.vo.PaymentMethodVO;
import cn.shoptnt.service.payment.PaymentManager;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;
import cn.shoptnt.framework.logs.Debugger;
import cn.shoptnt.framework.util.AbstractRequestUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * @description: 支付公用
 * @author: liuyulei
 * @create: 2019-12-30 19:46
 * @version:1.0
 * @since:7.1.4
 **/
@Tag(name = "交易支付API")
@RestController
@RequestMapping("/buyer/payment")
@Validated
public class PaymentController {


    @Autowired
    private Debugger debugger;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private DomainHelper domainHelper;


    @Operation(summary = "查询支持的支付方式")
    @Parameters({
            @Parameter(name = "client_type", description = "调用客户端PC,WAP,NATIVE,REACT", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{client_type}")
    public List<PaymentMethodVO> queryPayments(@PathVariable(name = "client_type") String clientType) {
        List<PaymentMethodVO> list = paymentManager.queryPayments(clientType);
        return list;
    }

    @Parameter(hidden = true)
    @Operation(summary = "接收支付异步回调")
    @RequestMapping(value = "/callback/{trade_type}/{plugin_id}/{client_type}")
    public String payCallback(@PathVariable(name = "trade_type") String tradeType, @PathVariable(name = "plugin_id") String paymentPluginId,
                              @PathVariable(name = "client_type") String clientType) {

        debugger.log("接收到回调消息");
        debugger.log("tradeType:[" + tradeType + "],paymentPluginId:[" + paymentPluginId + "],clientType:[" + clientType + "]");

        String result = this.paymentManager.payCallback(paymentPluginId, ClientType.valueOf(clientType));

        return result;
    }

    @Parameter(hidden = true)
    @Operation(summary = "接收支付同步回调")
    @GetMapping(value = "/return/{trade_type}/{pay_mode}/{client}/{sub_sn}/{plugin_id}", produces = MediaType.TEXT_HTML_VALUE)
    public String payReturn(@PathVariable(name = "trade_type") String tradeType, @PathVariable(name = "plugin_id") String paymentPluginId,
                            @PathVariable(name = "pay_mode") String payMode, @PathVariable(name = "client") String client,
                            @PathVariable(name = "sub_sn") String subSn, HttpServletResponse response) {

        this.paymentManager.payReturn(TradeTypeEnum.valueOf(tradeType), paymentPluginId);

        String serverName = domainHelper.getBuyerDomain();
        if (AbstractRequestUtil.isMobile()) {
            serverName = domainHelper.getMobileDomain();
        }

        String url = serverName + "/payment-complete?type=" + tradeType;
        //uniapp  h5
        if (ClientType.WAP.name().equals(client)) {
            if (TradeTypeEnum.RECHARGE.name().equals(tradeType)){
                url =serverName+ "/mine-module/account-balance?is_callback=yes";
            }else{
                url = serverName+"/order-module/cashier/cashier?is_callback=yes";
            }
            if (TradeTypeEnum.TRADE.name().equals(tradeType)) {
                url += "&trade_sn=" + subSn;
            } else {
                url += "&order_sn=" + subSn;
            }
        }

        String jumpHtml = "<script>";
        //扫码支付
        if (PayMode.qr.name().equals(payMode)) {
            //二维码模式嵌在的iframe中的，要设置此相应允许被buyer域名的frame嵌套
            jumpHtml += "window.parent.location.href='" + url + "'";
        } else {
            jumpHtml += "location.href='" + url + "'";
        }

        jumpHtml += "</script>";

        return jumpHtml;
    }

    @Operation(summary = "主动查询支付结果")
    @GetMapping(value = "/order/pay/query/{trade_type}/{sn}")
    public String query(@PathVariable(name = "trade_type") String tradeType, @Valid PayParam param,
                        @PathVariable(name = "sn") String sn) {

        String result = this.paymentManager.queryResult(sn, tradeType);

        return result;
    }


}
