/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.promotion.pintuan.PintuanOrderDetailVo;
import cn.shoptnt.service.trade.pintuan.PintuanOrderManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Created by 妙贤 on 2019-01-30.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-30
 */
@Tag(name = "拼团订单API")
@RestController
@RequestMapping("/buyer/pintuan")
public class PinTuanOrderController {

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @GetMapping("/orders/{order_sn}")
    @Operation(summary = "获取某个拼团的详细")
    @Parameters({
            @Parameter(name = "order_sn", description = "order_sn", required = true,   in = ParameterIn.PATH)

    })
    public PintuanOrderDetailVo detail(@Parameter(hidden = true) @PathVariable(name = "order_sn") String orderSn) {

        PintuanOrderDetailVo pintuanOrder = pintuanOrderManager.getMainOrderBySn(orderSn);

        return pintuanOrder;
    }

}
