/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import cn.shoptnt.model.member.dto.AppleIDUserDTO;
import cn.shoptnt.service.passport.LoginAppleIDManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * IOS-APP端 AppleID登录
 *
 * @author snow
 * @version v1.0
 * @since v7.2.2
 * 2020-12-16
 */
@Tag(name = "IOS-APP端 AppleID登录")
@RestController
@RequestMapping("/buyer/connect/apple")
@Validated
public class LoginAppleIDController {

    @Autowired
    private LoginAppleIDManager loginAppleIDManager;

    @Operation(summary = "APP登陆")
    @PostMapping("/app/login/{uuid}")
    public Map appLogin(@PathVariable String uuid, AppleIDUserDTO appleIDUserDTO){
        
        
        return this.loginAppleIDManager.appleIDLogin(uuid,appleIDUserDTO);
    }

}
