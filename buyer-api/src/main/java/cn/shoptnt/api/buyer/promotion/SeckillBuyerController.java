/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.promotion.seckill.vo.TimeLineVO;
import cn.shoptnt.service.promotion.seckill.SeckillGoodsManager;
import cn.shoptnt.service.promotion.seckill.SeckillRangeManager;
import cn.shoptnt.framework.exception.ServiceException;

import org.apache.bcel.verifier.statics.LONG_Upper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;

/**
 * 限时抢购相关API
 * @author Snow create in 2018/7/23
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/promotions/seckill")
@Tag(name = "限时抢购相关API")
@Validated
public class SeckillBuyerController {

    @Autowired
    private SeckillGoodsManager seckillApplyManager;

    @Autowired
    private SeckillRangeManager seckillRangeManager;

    @Operation(summary = "读取秒杀时刻")
    @ResponseBody
    @GetMapping(value = "/time-line")
    public List<TimeLineVO> readTimeLine(){
        List<TimeLineVO> timeLineVOList = this.seckillRangeManager.readTimeList();
        return timeLineVOList;
    }


    @Operation(summary = "根据参数读取限时抢购的商品列表")
    @Parameters({
            @Parameter(name	= "range_time", description ="时刻", in = ParameterIn.QUERY),
            @Parameter(name	= "page_no", description ="页码", 	in = ParameterIn.QUERY),
            @Parameter(name	= "page_size", description ="条数", 	in = ParameterIn.QUERY)
    })
    @GetMapping("/goods-list")
    public WebPage goodsList(@Parameter(hidden = true) Integer rangeTime, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long pageNo){

        if(rangeTime == null){
            throw new ServiceException(PromotionErrorCode.E400.code(),"时刻不能为空");
        }

        if(rangeTime > 24){
            throw new ServiceException(PromotionErrorCode.E400.code(),"时刻必须是0~24的整数");
        }

        List list = this.seckillApplyManager.getSeckillGoodsList(rangeTime,pageNo,pageSize);

        WebPage page = new WebPage();
        page.setData(list);
        page.setPageNo(pageNo.longValue());
        page.setPageSize(pageSize.longValue());
        page.setDataTotal(Integer.valueOf(list.size()).longValue());
        return page;

    }


}
