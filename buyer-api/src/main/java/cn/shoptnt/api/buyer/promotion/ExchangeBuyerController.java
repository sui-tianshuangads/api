/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.model.promotion.exchange.dos.ExchangeCat;
import cn.shoptnt.model.promotion.exchange.dos.ExchangeDO;
import cn.shoptnt.model.promotion.exchange.dto.ExchangeQueryParam;
import cn.shoptnt.service.promotion.exchange.ExchangeCatManager;
import cn.shoptnt.service.promotion.exchange.ExchangeGoodsManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;

/**
 * 积分商品相关API
 *
 * @author Snow create in 2018/7/23
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/promotions/exchange")
@Tag(name = "积分商品相关API")
@Validated
public class ExchangeBuyerController {

    @Autowired
    private ExchangeCatManager exchangeCatManager;

    @Autowired
    private ExchangeGoodsManager exchangeGoodsManager;

    @Operation(summary = "查询积分分类集合")
    @GetMapping("/cats")
    public List<ExchangeCat> getCat(){
        List<ExchangeCat> catList = this.exchangeCatManager.list(0L);
        return catList;
    }


    @Operation(summary = "查询积分商品")
    @Parameters({
            @Parameter(name	= "cat_id", description ="积分分类id", in = ParameterIn.QUERY),
            @Parameter(name	= "page_no", description ="页码", 	in = ParameterIn.QUERY),
            @Parameter(name	= "page_size", description ="条数", 	in = ParameterIn.QUERY)
    })
    @GetMapping("/goods")
    public WebPage<ExchangeDO> list(@Parameter(hidden = true) Long catId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        ExchangeQueryParam param = new ExchangeQueryParam();
        param.setCatId(catId);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        param.setStartTime(DateUtil.getDateline());
        param.setEndTime(DateUtil.getDateline());
        WebPage webPage = this.exchangeGoodsManager.list(param);
        return webPage;
    }

}
